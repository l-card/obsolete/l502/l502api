unit lpcieapi;

interface

const
  // ������������ ������ ������ � ��������� ����������
  LPCIE_DEVNAME_SIZE     = 32;
  // ������������ ������ ��������� ������ */
  LPCIE_SERIAL_SIZE      = 32;


  { �����, ����������� ������ }
    // ������� ������� ���
  LPCIE_DEVINFO_FLAGS_DAC_PRESENT = $0001;
  // ������� ������� ����������������
  LPCIE_DEVINFO_FLAGS_GAL_PRESENT = $0002;
  // ������� ������� ����������� ����������
  LPCIE_DEVINFO_FLAGS_BF_PRESENT  = $0004;
  // �������, ��� ���������� ��� �������
  LPCIE_DEVINFO_FLAGS_DEV_OPENED  = $0100;


  { �����, ������� ����� �������� � LPCIE_GetDevInfoList() }
  // �������, ��� ����� ������ ������ ����������, ������� ��� �� �������
  LPCIE_GETDEVS_FLAGS_ONLY_NOT_OPENED = $0001;

implementation

end.
 
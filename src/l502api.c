#include "l502_private.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "timer.h"

#ifdef _WIN32
int WINAPI DllEntryPoint
(
        HINSTANCE hinst,
        unsigned long reason,
        void* lpReserved
        )
{
    return 1;
}
#endif

static const double f_scales[] = {10., 5., 2., 1., 0.5, 0.2};
static const char f_devname[] = "L502";

typedef enum
{
    STREAM_IN_WRD_ADC  = 0,
    STREAM_IN_WRD_DIN  = 1,
    STREAM_IN_WRD_MSG  = 2,
    STREAM_IN_WRD_USR  = 3
} t_stream_in_wrd_type;


LPCIE_EXPORT(int32_t) LPCIE_FreeDevInfoList(t_lpcie_devinfo *list, uint32_t size)
{
    uint32_t i;
    for (i=0; i < size; i++)
    {
        _free_intptr(list[i].intptr);
        free(list[i].intptr);
        list[i].intptr = NULL;
    }
    return 0;
}


LPCIE_EXPORT(int32_t) L502_GetSerialList(char serials[][L502_SERIAL_SIZE], uint32_t size,
                           uint32_t flags, uint32_t* devcnt)
{

    uint32_t fnd_cnt=0, l502_cnt=0;
    /* получаем количество устройств, поддерживаемых драйвером lpcie */
    int32_t res = LPCIE_GetDevInfoList(NULL, 0, flags, &fnd_cnt);
    if ((res>=0) && fnd_cnt)
    {
        t_lpcie_devinfo *info_list = malloc(sizeof(t_lpcie_devinfo)*fnd_cnt);
        if (info_list==NULL)
        {
            res = L502_ERR_MEMORY_ALLOC;
        }
        else
        {
            /* получаем информацию по всем устройствам драйвера lpcie */
            res = LPCIE_GetDevInfoList(info_list, fnd_cnt, flags, NULL);
            if (res>0)
            {
                int32_t i;
                for (i=0; i < res; i++)
                {
                    /* проверяем, что это устройство - L502 */
                    if (!strcmp(info_list[i].devname, f_devname))
                    {
                        /* если есть место в списке, то сохраняем серийный номер
                           устройства */
                        if (l502_cnt < size)
                        {
                            memcpy(serials[l502_cnt], info_list[i].serial,
                                   L502_SERIAL_SIZE);
                        }
                        l502_cnt++;
                    }
                }
            }

            LPCIE_FreeDevInfoList(info_list, fnd_cnt);
            free(info_list);
        }
    }

    if (devcnt!=NULL)
        *devcnt = l502_cnt;

    return res < 0 ? res : l502_cnt > size ? (int32_t)size : (int32_t)l502_cnt;
}


LPCIE_EXPORT(int32_t) L502_Open(t_l502_hnd hnd, const char* serial)
{
    int32_t err = L502_CHECK_HND(hnd);
    int32_t get_info_res = 0;
    uint32_t fnd_cnt;

    if (!err)
    {
        get_info_res = LPCIE_GetDevInfoList(NULL, 0, 0, &fnd_cnt);
        if (get_info_res < 0)
            err = get_info_res;
        else if (!fnd_cnt)
            err = L502_ERR_DEVICE_NOT_FOUND;
    }
    if (!err)
    {
        t_lpcie_devinfo *info_list = malloc(sizeof(t_lpcie_devinfo)*fnd_cnt);
        if (info_list==NULL)
        {
            err = L502_ERR_MEMORY_ALLOC;
        }
        else
        {
            /* получаем информацию по всем устройствам драйвера lpcie */
            get_info_res = LPCIE_GetDevInfoList(info_list, fnd_cnt, 0, NULL);
            if (get_info_res < 0)
            {
                err = get_info_res;
            }
            else
            {
                int32_t i, ser_size=0, fnd=0, open_err = 0;

                if (serial!=NULL)
                {
                    /* если серийный задан - смотрим его размер, отсекая
                     * все что начинается с признака кноца строки */
                    for (ser_size=0; (ser_size < L502_SERIAL_SIZE) &&
                         !((serial[ser_size]=='\0') ||
                           (serial[ser_size]=='\n') ||
                           (serial[ser_size]=='\r')); ser_size++)
                    {}
                }

                for (i=0; !fnd && (i < get_info_res); i++)
                {
                    /* ищем устройство L502 с совпадающим серийным */
                    if ((!strcmp(info_list[i].devname, f_devname)) &&
                            ((ser_size==0) || !strncmp(serial, info_list[i].serial,
                                                        ser_size)))
                    {
                        /* пробуем открыть устройство */
                        err = L502_OpenByListItem(hnd, &info_list[i]);

                        /* если серийный номер не был указан, то сохраняем
                           код ошибки и идем дальше, пробовать открыть
                           следующее устройство */
                        if (err && (ser_size==0))
                        {
                            open_err = err;
                            err = 0;
                        }
                        else
                        {
                            /* иначе заканчиваем поиск */
                            fnd = 1;
                        }
                    }
                }

                /* если не нашли устройство - устанавливаем соотвествующий код
                   ошибки */
                if (!fnd)
                {
                    err = open_err ? open_err : L502_ERR_DEVICE_NOT_FOUND;
                }
            }


            LPCIE_FreeDevInfoList(info_list, fnd_cnt);
            free(info_list);
        }
    }
    return err;

}








LPCIE_EXPORT(int32_t) L502_OpenByListItem(t_l502* hnd, t_lpcie_devinfo* info)
{
    int32_t err  = L502_CHECK_HND(hnd);
    if (!err && !info)
        err = L502_ERR_INVALID_POINTER;
    if (!err && (hnd->flags & _FLAGS_OPENED))
        err = L502_ERR_ALREADY_OPENED;
    if (!err)
        err = _open_by_list_intptr(hnd, info->intptr);

    if (!err)
    {

        L502_SetLChannel(hnd, 0, 0, L502_LCH_MODE_COMM, L502_ADC_RANGE_10, 0);
        hnd->set.lch_cnt = 1;
        hnd->set.adc_freq_div = 1;
        hnd->set.adc_frame_delay = 0;
        hnd->set.sync_mode = L502_SYNC_INTERNAL;
        hnd->set.sync_mode = L502_SYNC_INTERNAL;
        hnd->set.sync_start_mode = L502_SYNC_INTERNAL;

        hnd->set.din_freq_div = 1;
        hnd->set.dac_freq_div = 2;

        hnd->set.ref_freq = L502_REF_FREQ_2000KHZ;
        hnd->streams = 0;

        hnd->flags = _FLAGS_OPENED;


        /* читаем информацию о версии прошивки ПЛИС'ов и наличии опций */
        if (!err)
        {
            uint32_t hard_id=0;
            err = _fpga_reg_read(hnd, L502_REGS_HARD_ID, &hard_id);
            if (!err)
            {
                hnd->info.fpga_ver = (hard_id >> 16) & 0xFFFF;
                hnd->info.plda_ver = (hard_id >> 4) & 0xF;
                hnd->info.board_rev  = (hard_id >> 8) & 0xF;
                if (hard_id & 0x01)
                    hnd->info.devflags |= L502_DEVFLAGS_DAC_PRESENT;
                if (hard_id & 0x02)
                    hnd->info.devflags |= L502_DEVFLAGS_GAL_PRESENT;
                if (hard_id & 0x04)
                    hnd->info.devflags |= L502_DEVFLAGS_BF_PRESENT;
            }
        }

        /* определяем - в каком режиме работаем (BF или FPGA) */
        if (!err)
        {
            uint32_t bf_ctl;
            err = _fpga_reg_read(hnd, L502_REGS_BF_CTL, &bf_ctl);
            if (!err)
            {
                uint32_t mode = bf_ctl;
                if (mode & L502_REGBIT_BF_CTL_DBG_MODE_Msk)
                    hnd->mode = L502_MODE_DEBUG;
                else if (mode & L502_REGBIT_BF_CTL_DSP_MODE_Msk)
                    hnd->mode = L502_MODE_DSP;
                else
                    hnd->mode = L502_MODE_FPGA;
            }

            /** @todo Для BlackFin проверить наличие прошивки */

            if (hnd->mode==L502_MODE_DSP)
                err = _fpga_reg_write(hnd, L502_REGS_BF_CMD, L502_BF_CMD_HDMA_RST);
        }

        /* если был запущен сбор - то останавливаем его */
        if (!err && (hnd->mode==L502_MODE_FPGA))
        {

            err = _fpga_reg_write(hnd, L502_REGS_IOHARD_GO_SYNC_IO, 0);
            hnd->last_dout = 0;

            if (!err)
                err =  _fpga_reg_write(hnd, L502_REGS_IOHARD_OUTSWAP_BFCTL, 0);
#if 0
            /* читаем текущее состояние выходов, чтобы потом можно было наложить
             *  маску */
            if (!err)
            {
                err = _fpga_reg_read(hnd, L502_REGS_IOHARD_DIGOUT_ASYN_OUT,
                                     &hnd->last_dout);
            }
#endif
        }


        if (!err && (hnd->info.devflags & L502_DEVFLAGS_BF_PRESENT))
        {
            hnd->mutex_bf = osspec_mutex_create();
            if (hnd->mutex_bf == L_INVALID_MUTEX)
                err = L502_ERR_MUTEX_CREATE;
        }
        else
        {
            hnd->mutex_bf = L_INVALID_MUTEX;
        }

        if (!err)
        {
            hnd->mutex_cfg = osspec_mutex_create();
            if (hnd->mutex_cfg == L_INVALID_MUTEX)
                err = L502_ERR_MUTEX_CREATE;
        }
    }

    /* читаем информацию из EEPROM (в первую очередь -
       калибровочные коэффициенты) */
    if (!err)
    {
        int i;

        for (i=0; i < L502_ADC_RANGE_CNT; i++)
        {
            hnd->info.cbr.adc[i].offs = 0;
            hnd->info.cbr.adc[i].k = 1.;
        }

        for (i=0; i < L502_DAC_CH_CNT; i++)
        {
            hnd->info.cbr.dac[i].offs = 0;
            hnd->info.cbr.dac[i].k = 1;
        }

        _check_eeprom(hnd);
    }

    /* записываем конфигурацию по умолчанию, чтобы быть уверенным,
       что установлена нужная конфигурация */
    if (!err && (hnd->mode==L502_MODE_FPGA))
        err = L502_Configure(hnd, 0);
    //if (!err)
    //    err = _fpga_reg_write(hnd, L502_REGS_IOHARD_PRELOAD_ADC, 1);

    if (err)
        L502_Close(hnd);
    return err;
}








LPCIE_EXPORT(int32_t) L502_Close(t_l502_hnd hnd)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err && (hnd->flags  & _FLAGS_OPENED))
    {
        int32_t stop_err;
        if (hnd->flags & _FLAGS_STREAM_RUN)
        {
            /* остановка потока */
            err = L502_StreamsStop(hnd);
        }

        hnd->flags &= ~_FLAGS_OPENED;

        if (hnd->mutex_cfg!=L_INVALID_MUTEX)
        {
            stop_err = osspec_mutex_close(hnd->mutex_cfg);
            hnd->mutex_cfg = L_INVALID_MUTEX;
            if (!err)
                err = stop_err;
        }

        if (hnd->mutex_bf!=L_INVALID_MUTEX)
        {
            stop_err = osspec_mutex_close(hnd->mutex_bf);
            hnd->mutex_bf = L_INVALID_MUTEX;
            if (!err)
                err = stop_err;
        }

        _dev_close(hnd);


    }
    return err;
}

LPCIE_EXPORT(t_l502_hnd) L502_Create(void)
{
    t_l502* hnd = malloc(sizeof(t_l502));
    if (hnd!=NULL)
    {
        memset(hnd, 0, sizeof(t_l502));
        hnd->sign = L502_SIGN;
    }
    return hnd;
}

LPCIE_EXPORT(int32_t) L502_Free(t_l502_hnd hnd)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err)
    {
        if (hnd->flags & _FLAGS_OPENED)
            err = L502_Close(hnd);

        hnd->sign = 0;
        free(hnd);
    }
    return err;
}


LPCIE_EXPORT(int32_t) L502_GetDevInfo(t_l502_hnd hnd, t_l502_info* info)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err && (info==NULL))
        err = L502_ERR_INVALID_POINTER;
    if (!err)
        *info = hnd->info;
    return err;
}



#define STREAM_IN_WRD_TYPE(wrd) wrd & 0x80000000 ? STREAM_IN_WRD_ADC : \
      (wrd & 0xFF000000) == 0x0 ? STREAM_IN_WRD_DIN : \
    ((wrd & 0xFF000000)>>24) == 0x01 ? STREAM_IN_WRD_MSG : STREAM_IN_WRD_USR


LPCIE_EXPORT(int32_t) L502_ProcessAdcData(t_l502_hnd hnd, const uint32_t* src,  double *dest,
                            uint32_t *size, uint32_t flags)
{
    if (size == NULL)
        return L502_ERR_INVALID_POINTER;
    return L502_ProcessDataWithUserExt(hnd, src, *size, flags, dest, size,
                                        NULL, NULL, NULL, NULL);
}

LPCIE_EXPORT(int32_t) L502_ProcessData(t_l502_hnd hnd, const uint32_t *src, uint32_t size, uint32_t flags,
                           double *adc_data, uint32_t *adc_data_size,
                           uint32_t *din_data, uint32_t *din_data_size)
{
    return L502_ProcessDataWithUserExt(hnd, src, size, flags, adc_data, adc_data_size,
                                      din_data, din_data_size, NULL, NULL);
}


LPCIE_EXPORT(int32_t) L502_ProcessDataWithUserExt(t_l502_hnd hnd, const uint32_t* src, uint32_t size,
                                   uint32_t flags, double *adc_data,
                                   uint32_t *adc_data_size, uint32_t *din_data, uint32_t *din_data_size,
                                   uint32_t *usr_data, uint32_t *usr_data_size)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err && (adc_data_size==NULL) && (adc_data!=NULL))
        err = L502_ERR_INVALID_POINTER;
    if (!err && (din_data_size==NULL) && (din_data!=NULL))
        err = L502_ERR_INVALID_POINTER;
    if (!err && (usr_data_size==NULL) && (usr_data!=NULL))
        err = L502_ERR_INVALID_POINTER;
    if (!err)
    {
        uint32_t adc_cnt = 0, din_cnt=0, usr_cnt = 0;
        uint32_t i;

        for (i=0; (i<size) && !err; i++)
        {
            register uint32_t wrd = src[i];
            t_stream_in_wrd_type type = STREAM_IN_WRD_TYPE(wrd);
            /* проверяем - это данные от АЦП или цифровых входов */
            switch (type)
            {
            case STREAM_IN_WRD_ADC:
                {
                    uint32_t ch_num = (wrd >> 24) & 0xF;
                    uint32_t ch_mode = (wrd >> 28) & 0x3;
                    int32_t val;
                    uint32_t range;

                    /* проверяем совпадение каналов */
                    switch (ch_mode)
                    {
                        case 0:
                            ch_mode = L502_LCH_MODE_DIFF;
                            break;
                        case 1:
                            ch_mode = L502_LCH_MODE_COMM;
                            break;
                        case 2:
                            ch_mode = L502_LCH_MODE_COMM;
                            ch_num+=16;
                            break;
                        case 3:
                            ch_mode = L502_LCH_MODE_ZERO;
                            break;
                    }

                    if (!(flags & L502_PROC_FLAGS_DONT_CHECK_CH) &&
                            ((hnd->set.lch[hnd->proc_adc_ch].mode != ch_mode) ||
                            (hnd->set.lch[hnd->proc_adc_ch].ch != ch_num)))
                    {
                        err = L502_ERR_PROC_INVALID_CH_NUM;    
                    }
                    else
                    {
                        /* проверяем формат - пришло откалиброванное 24-битное слово,
                          или неоткалиброванное 16-битное */
                        if (wrd & 0x40000000)
                        {
                            val = wrd & 0xFFFFFF;
                            if (wrd & 0x800000)
                                val |= 0xFF000000;

                            range = hnd->set.lch[hnd->proc_adc_ch].range;
                        }
                        else
                        {
                            range = (wrd >> 16) & 0x7;
                            if (!(flags & L502_PROC_FLAGS_DONT_CHECK_CH) &&
                                    (range != hnd->set.lch[hnd->proc_adc_ch].range))
                            {
                                err = L502_ERR_PROC_INVALID_CH_RANGE;
                            }
                            else
                            {
                                val = wrd & 0xFFFF;
                                if (wrd & 0x8000)
                                    val |= 0xFFFF0000;
                            }
                        }
                    }

                    if (!err)
                    {
                        double res_val = val;
                        if (flags & L502_PROC_FLAGS_VOLT)
                        {
                            res_val = f_scales[range]*res_val/L502_ADC_SCALE_CODE_MAX;
                        }


                        if ((adc_data!=NULL) && (adc_cnt<*adc_data_size))
                        {
                            *adc_data++ = res_val;
                            adc_cnt++;
                        }
                        else if (adc_data==NULL)
                        {
                            adc_cnt++;
                        }

                        if (++hnd->proc_adc_ch ==hnd->set.lch_cnt)
                            hnd->proc_adc_ch = 0;
                    }
                }
                break;
            case STREAM_IN_WRD_DIN:
                if ((din_data!=NULL) && (din_cnt<*din_data_size))
                {
                    *din_data++ = wrd & 0x3FFFF;
                    din_cnt++;
                }
                else if (din_data==NULL)
                {
                    din_cnt++;
                }
                break;
            case STREAM_IN_WRD_MSG:
                err = wrd==L502_STREAM_IN_MSG_OVERFLOW ? L502_ERR_STREAM_OVERFLOW :
                                                         L502_ERR_UNSUP_STREAM_MSG;
                break;
            case STREAM_IN_WRD_USR:
                if ((usr_data!=NULL) && (usr_cnt<*usr_data_size))
                {
                    *usr_data++ = wrd;
                    usr_cnt++;
                }
                else if (usr_data==NULL)
                {
                    usr_cnt++;
                }
                break;
            }
        }


        if (adc_data_size!=NULL)
            *adc_data_size = adc_cnt;
        if (din_data_size!=NULL)
            *din_data_size = din_cnt;
        if (usr_data_size!=NULL)
            *usr_data_size = usr_cnt;
    }

    return err;
}

static uint32_t f_prepare_dac_wrd(t_l502_hnd hnd, double val,
                                  uint32_t flags, const t_l502_cbr_coef* coef)
{
    int32_t wrd = 0;
    if (flags & L502_DAC_FLAGS_VOLT)
    {
        val = (val/L502_DAC_RANGE)*L502_DAC_SCALE_CODE_MAX;
    }
    if (flags & L502_DAC_FLAGS_CALIBR)
    {
        val = (val+coef->offs)*coef->k;
    }
    wrd = (int32_t)val;
    wrd &= 0xFFFF;
    return wrd;
}

LPCIE_EXPORT(int32_t) L502_PrepareData(t_l502_hnd hnd, const double* dac1, const double* dac2,
                            const uint32_t* digout, uint32_t size, int32_t flags,
                            uint32_t* out_buf)
{
    int err = L502_CHECK_HND(hnd);
    if (!err && (out_buf==NULL))
        err = L502_ERR_INVALID_POINTER;
    if (!err && ((dac1==NULL) && (dac2==NULL) && (digout==NULL)))
        err = L502_ERR_INVALID_POINTER;


    if (!err)
    {
        uint32_t i;
        for (i=0; (i < size) && !err; i++)
        {
            if ((dac1!=NULL) && (hnd->streams&L502_STREAM_DAC1))
            {
                uint32_t wrd = f_prepare_dac_wrd(hnd, *dac1++, flags, &hnd->info.cbr.dac[0]);
                *out_buf++ = wrd | L502_STREAM_OUT_WORD_TYPE_DAC1;
            }
            if ((dac2!=NULL) && (hnd->streams&L502_STREAM_DAC2))
            {
                uint32_t wrd = f_prepare_dac_wrd(hnd, *dac2++, flags, &hnd->info.cbr.dac[1]);
                *out_buf++ = wrd | L502_STREAM_OUT_WORD_TYPE_DAC2;
            }
            if ((digout!=NULL) && (hnd->streams&L502_STREAM_DOUT))
            {
                uint32_t wrd = *digout++;
                *out_buf++ = (wrd &0x3FFFF)
                                | L502_STREAM_OUT_WORD_TYPE_DOUT;
            }
        }
    }
    return err;
}

LPCIE_EXPORT(int32_t) L502_GetNextExpectedLchNum(t_l502_hnd hnd, uint32_t *lch)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err && (lch==NULL))
        err = L502_ERR_INVALID_POINTER;
    if (!err)
        *lch = hnd->proc_adc_ch;
    return err;
}

LPCIE_EXPORT(int32_t) L502_GetRecvReadyCount(t_l502_hnd hnd, uint32_t *rdy_cnt)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err && (rdy_cnt==NULL))
        err = L502_ERR_INVALID_POINTER;
    if (!err)
        err = _stream_rdy_size(hnd, L502_DMA_CHNUM_IN, rdy_cnt);
    return err;
}

LPCIE_EXPORT(int32_t) L502_GetSendReadyCount(t_l502_hnd hnd, uint32_t *rdy_cnt)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err && (rdy_cnt==NULL))
        err = L502_ERR_INVALID_POINTER;
    if (!err)
        err = _stream_rdy_size(hnd, L502_DMA_CHNUM_OUT, rdy_cnt);
    return err;
}

static int32_t f_check_dma_ch_par_en(t_l502_hnd hnd, uint32_t dma_ch)
{
    int32_t err = 0;
    switch (dma_ch)
    {
        case L502_DMA_CH_IN:
            if ((hnd->flags & _FLAGS_STREAM_RUN)
                    && (hnd->streams & L502_STREAM_ALL_IN))
            {
                err = L502_ERR_STREAM_IS_RUNNING;
            }
            break;
        case L502_DMA_CH_OUT:
            if ((hnd->flags & (_FLAGS_PRELOAD_DONE | _FLAGS_STREAM_RUN))
                    && (hnd->streams & L502_STREAM_ALL_OUT))
            {
                err = L502_ERR_STREAM_IS_RUNNING;
            }
            break;
        default:
            err = L502_ERR_INVALID_DMA_CH;
    }
    return err;
}

LPCIE_EXPORT(int32_t) L502_SetDmaBufSize(t_l502_hnd hnd, uint32_t dma_ch, uint32_t size)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err)
        err = f_check_dma_ch_par_en(hnd, dma_ch);
    if (!err)
        hnd->dma_params[dma_ch].buf_size = size;
    return err;
}

LPCIE_EXPORT(int32_t) L502_SetDmaIrqStep(t_l502_hnd hnd, uint32_t dma_ch, uint32_t step)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err)
        err = f_check_dma_ch_par_en(hnd, dma_ch);
    if (!err)
        hnd->dma_params[dma_ch].irq_step = step;
    return err;
}



LPCIE_EXPORT(int32_t) L502_AsyncOutDac(t_l502_hnd hnd, uint32_t ch, double data, uint32_t flags)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err && (ch!=L502_DAC_CH1) && (ch!=L502_DAC_CH2))
        err = L502_ERR_INVALID_DAC_CHANNEL;
    if (!err && !(hnd->info.devflags & L502_DEVFLAGS_DAC_PRESENT))
        err = L502_ERR_DAC_NOT_PRESENT;
    if (!err)
    {
        uint32_t wr_val = f_prepare_dac_wrd(hnd, data, flags, &hnd->info.cbr.dac[ch]);

        if (ch==L502_DAC_CH1)
            wr_val |= L502_STREAM_OUT_WORD_TYPE_DAC1;
        else
            wr_val |= L502_STREAM_OUT_WORD_TYPE_DAC2;

        if (hnd->mode == L502_MODE_FPGA)
        {
            err = _fpga_reg_write(hnd, L502_REGS_IOHARD_ASYNC_OUT, wr_val);
        }
        else if (hnd->mode == L502_MODE_DSP)
        {
            err = L502_BfExecCmd(hnd, L502_BF_CMD_CODE_ASYNC_OUT,  ch==L502_DAC_CH1 ?
                               L502_BF_CMD_ASYNC_TYPE_DAC1 : L502_BF_CMD_ASYNC_TYPE_DAC2,
                           (uint32_t*)&wr_val, 1, NULL, 0, L502_BF_CMD_DEFAULT_TOUT, NULL);
        }
        else
        {
            err = L502_ERR_INVALID_MODE;
        }
    }
    return err;
}

LPCIE_EXPORT(int32_t) L502_AsyncOutDig(t_l502_hnd hnd, uint32_t val, uint32_t msk)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err)
    {
        if(hnd->mode == L502_MODE_FPGA)
        {
            err = osspec_mutex_lock(hnd->mutex_cfg, L502_CFG_LOCK_MUTEX_TOUT);
            if (!err)
            {
                int32_t release_err;
                /* маскированные биты берем из последнего выведенного значения */
                if (msk)
                {
                    val = val & ~msk;
                    val |= hnd->last_dout & msk;
                }

                err = _fpga_reg_write(hnd, L502_REGS_IOHARD_ASYNC_OUT,
                                      val | L502_STREAM_OUT_WORD_TYPE_DOUT);
                /* сохраняем выведенное значения, для последующего использования
                   в маске */
                if (!err)
                    hnd->last_dout = val;

                release_err = osspec_mutex_release(hnd->mutex_cfg);
                if (!err)
                    err = release_err;
            }
        }
        else if (hnd->mode == L502_MODE_DSP)
        {
            uint32_t wrds[2] = {val, msk};
            err = L502_BfExecCmd(hnd, L502_BF_CMD_CODE_ASYNC_OUT, L502_BF_CMD_ASYNC_TYPE_DOUT,
                                 wrds, 2, NULL, 0, L502_BF_CMD_DEFAULT_TOUT, NULL);
        }
        else
        {
            err = L502_ERR_INVALID_MODE;
        }
    }
    return err;
}


static int32_t f_read_digin(t_l502_hnd hnd, uint32_t* din)
{
    int32_t err = 0;
    uint32_t val;
    int rdy=0;
    t_timer tmr;

    /* читаем состояние входов, чтобы сбросить флаг готовности */
    err = _fpga_reg_read(hnd, L502_REGS_IOARITH_DIN_ASYNC, &val);

    /* читаем синхронный ввод до готовности данных или пока не
       выйдем по таймауту*/
    timer_set(&tmr, 500*CLOCK_CONF_SECOND/1000);
    while (!rdy && !timer_expired(&tmr) && !err)
    {
        err = _fpga_reg_read(hnd, L502_REGS_IOARITH_DIN_ASYNC, &val);
        if (!err && (val&0x80000000))
        {
            rdy = 1;
            *din = (val & 0x3FFFF);
        }
    }

    if (!err && !rdy)
        err = L502_ERR_DIG_IN_NOT_RDY;

    return err;
}



LPCIE_EXPORT(int32_t) L502_AsyncInDig(t_l502_hnd hnd, uint32_t* din)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err & (din==NULL))
        err = L502_ERR_INVALID_POINTER;
    if (!err)
         err = osspec_mutex_lock(hnd->mutex_cfg, L502_CFG_LOCK_MUTEX_TOUT);
    if (!err)
    {
        if (hnd->mode == L502_MODE_FPGA)
        {
            if (hnd->flags & _FLAGS_STREAM_RUN)
            {
                err = f_read_digin(hnd, din);
            }
            else
            {
                /* запрещаем прием данных */
                err = _fpga_reg_write(hnd, L502_REGS_IOARITH_IN_STREAM_ENABLE, 0);
                if (!err)
                {
                    err = _fpga_reg_write(hnd, L502_REGS_IOHARD_PRELOAD_ADC, 1);
                    if (!err)
                    {
                        /* запускаем чтение цифровых входов */
                        err = _fpga_reg_write(hnd, L502_REGS_IOHARD_GO_SYNC_IO, 1);
                    }
                }

                if (!err)
                {
                    err = f_read_digin(hnd, din);

                    /* останавливаем сбор данных */
                    _fpga_reg_write(hnd, L502_REGS_IOHARD_GO_SYNC_IO, 0);
                }
            }
        }
        else if (hnd->mode == L502_MODE_DSP)
        {
            err = L502_BfExecCmd(hnd, L502_BF_CMD_CODE_ASYNC_DIG_IN,
                                 0, NULL, 0, din, 1, L502_BF_CMD_DEFAULT_TOUT, NULL);
        }
        else
        {
            err = L502_ERR_INVALID_MODE;
        }
        osspec_mutex_release(hnd->mutex_cfg);
    }
    return err;
}






LPCIE_EXPORT(int32_t) L502_FpgaRegWrite(t_l502_hnd hnd, uint32_t reg, uint32_t val)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err)
        err = _fpga_reg_write(hnd, reg, val);
    return err;
}

LPCIE_EXPORT(int32_t) L502_FpgaRegRead(t_l502_hnd hnd, uint32_t reg, uint32_t *val)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err)
        err = _fpga_reg_read(hnd, reg, val);
    return err;
}

LPCIE_EXPORT(uint32_t) L502_GetDllVersion(void)
{
    return (L502API_VER_MAJOR << 24) | (L502API_VER_MINOR<<16) |
            (L502API_VER_PATCH << 8);
}

LPCIE_EXPORT(int32_t) L502_GetDriverVersion(t_l502_hnd hnd, uint32_t* ver)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err)
        err = _get_drv_ver(hnd, ver);
    return err;
}

LPCIE_EXPORT(int32_t) L502_LedBlink(t_l502_hnd hnd)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err)
    {
        if (hnd->mode == L502_MODE_DSP)
        {
            err = L502_ERR_NOT_IMPLEMENTED;
        }
        else
        {
            int32_t err2;
            err = _fpga_reg_write(hnd, L502_REGS_IOHARD_LED, 0);
            SLEEP_MS(200);
            err2 = _fpga_reg_write(hnd, L502_REGS_IOHARD_LED, 1);
            if (!err)
                err = err2;
        }
    }
    return err;
}

LPCIE_EXPORT(int32_t) L502_SetDigInPullup(t_l502_hnd hnd, uint32_t pullups)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err)
    {
        if (hnd->mode==L502_MODE_FPGA)
        {
            uint32_t val = 0;
            if (pullups & L502_PULLUPS_DI_L)
                val |= 0x1;
            if (pullups & L502_PULLUPS_DI_H)
                val |= 0x2;
            if (pullups & L502_PULLUPS_DI_SYN1)
                val |= 0x4;
            if (pullups & L502_PULLUPS_DI_SYN2)
                val |= 0x8;
            err = _fpga_reg_write(hnd, L502_REGS_IOHARD_DIGIN_PULLUP, val);
        }
        else if (hnd->mode == L502_MODE_DSP)
        {
            err = L502_ERR_NOT_IMPLEMENTED;
        }
        else
        {
            err = L502_ERR_INVALID_MODE;
        }
    }
    return err;
}

LPCIE_EXPORT(int32_t) L502_ReloadDevInfo(t_l502_hnd hnd)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err)
        err = _renew_info(hnd);
    if (!err)
        err = _check_eeprom(hnd);
    return err;
}

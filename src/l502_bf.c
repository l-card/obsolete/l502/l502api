#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>

#include "l502_private.h"
#include "timer.h"
#include "l502_bf_cmd_defs.h"

#define BF_LDR_HDR_SIZE  (16)
#define BF_LDR_HDRSGN    (0xAD)

#define BF_LDR_HDRPOS_SGN  (3)

#define BF_LDR_FLAG_SAVE      (0x0010) //не используется
#define BF_LDR_FLAG_AUX       (0x0020) //не используется
#define BF_LDR_FLAG_FILL      (0x0100)
#define BF_LDR_FLAG_QUICKBOOT (0x0200) //не используется
#define BF_LDR_FLAG_CALLBACK  (0x0400) //не используется
#define BF_LDR_FLAG_INIT      (0x0800) //не используется
#define BF_LDR_FLAG_IGNORE    (0x1000)
#define BF_LDR_FLAG_INDIRECT  (0x2000) //не используется
#define BF_LDR_FLAG_FIRST     (0x4000)
#define BF_LDR_FLAG_FINAL     (0x8000)

#define L502_BF_WAIT_LOAD_RDY_TOUT  500

#define LDR_BUFF_SIZE 4096


#define BF_CHECK_ADDR(addr)  (((addr) < 0xFFA0C000) && ((addr)>= 0xFFA0000)) || \
    (((addr) < 0xFF908000) && ((addr) >=0xFF900000)) || \
    (((addr) < 0xFF808000) && ((addr) >=0xFF800000)) || \
    (((addr) < 0x2000000)) ? 0 : L502_ERR_BF_INVALID_ADDR

#define BF_CHECK_ADDR_SIZE(addr, size) BF_CHECK_ADDR(addr) ? L502_ERR_BF_INVALID_ADDR : \
    BF_CHECK_ADDR(addr+size*4-1) ? L502_ERR_BF_INVALID_ADDR : 0


#define BF_CMD_FIRST_DATA_BLOCK_SIZE ((L502_BF_REQ_DATA_SIZE_MAX-offsetof(t_l502_bf_cmd, data))/4)

typedef struct st_bf_ldr_pkt
{
    uint8_t res;
    uint8_t dma_mode;
    uint16_t flags;
    uint32_t addr;
    uint32_t size;
    uint32_t arg;
} t_bf_ldr_pkt;

/* Разбираем заголовок блока LDR-формата из буфера размером BF_LDR_HDR_SIZE
   и сохраняем параметры в структуре pkt */
int f_parse_ldr_hdr(const uint8_t* hdr, t_bf_ldr_pkt* pkt)
{
    int err = 0;
    uint32_t* pdw_buff = (uint32_t*)hdr;
    uint8_t xor_ch = 0;
    int i;
    for (i=0; i < BF_LDR_HDR_SIZE; i++)
    {
        xor_ch ^= hdr[i];
    }

    if ((xor_ch!=0) || (hdr[BF_LDR_HDRPOS_SGN] != BF_LDR_HDRSGN))
    {
        err = L502_ERR_LDR_FILE_FORMAT;
    }
    else
    {
        pkt->res = 0;
        pkt->dma_mode = pdw_buff[0]&0xF;
        pkt->flags = pdw_buff[0]&0xFFF0;
        pkt->addr = pdw_buff[1];
        pkt->size = pdw_buff[2];
        pkt->arg = pdw_buff[3];

        if ((pkt->flags & BF_LDR_FLAG_INIT) && (pkt->flags & BF_LDR_FLAG_FILL))
            err = L502_ERR_LDR_FILE_FORMAT;
        else if (pkt->flags & (BF_LDR_FLAG_CALLBACK | BF_LDR_FLAG_INDIRECT | BF_LDR_FLAG_INIT))
            err = L502_ERR_LDR_FILE_UNSUP_FEATURE;
        else if ((pkt->flags & BF_LDR_FLAG_INIT) && (pkt->addr != 0xFFA00000))
            err = L502_ERR_LDR_FILE_UNSUP_STARTUP_ADDR;
    }
    return err;
}

static int32_t f_bf_wait_cmd_done(t_l502_hnd hnd)
{
    int32_t err = 0;
    t_timer tmr;
    uint32_t status;
    timer_set(&tmr, L502_BF_REQ_TOUT*CLOCK_CONF_SECOND/1000);
    do
    {
        err = _fpga_reg_read(hnd, L502_REGS_BF_STATUS, &status);
    } while ((status &  L502_REGBIT_BF_STATUS_BUSY_Msk) &&
             !err && !timer_expired(&tmr));

    if (!err && (status & L502_REGBIT_BF_STATUS_BUSY_Msk))
        err = L502_ERR_BF_REQ_TIMEOUT;
    return err;
}

static int32_t f_bf_mem_wr(t_l502_hnd hnd, uint32_t addr, const uint32_t* regs, uint32_t size)
{
    int32_t err = 0;

    if (!err)
        err = f_bf_wait_cmd_done(hnd);

    /* данные записываем блоками по L502_BF_REQ_DATA_SIZE */
    while (!err && size)
    {
        int i;
        int put_size = (size < L502_BF_REQ_DATA_SIZE_MAX) ? size :
                                         L502_BF_REQ_DATA_SIZE_MAX;
        /* записываем блок данных в буфер ПЛИС */
        for (i=0; (i < put_size) && !err; i++)
        {
            err = _fpga_reg_write(hnd, L502_REGS_BF_REQ_DATA+i, regs[i]);
        }

        /* записываем переметры передачи - размер и адрес в памяти BlackFin */
        if (!err)
            err = _fpga_reg_write(hnd,  L502_REGS_BF_REQ_SIZE, put_size);
        if (!err)
            err = _fpga_reg_write(hnd, L502_REGS_BF_REQ_ADDR, addr);
        /* даем команду на запис */
        if (!err)
            err = _fpga_reg_write(hnd, L502_REGS_BF_CMD, L502_BF_CMD_WRITE);

        /* ждем, пока операция не будет завершена */
        if (!err)
            err = f_bf_wait_cmd_done(hnd);

        if (!err)
        {
            size -= put_size;
            regs += put_size;
            addr += put_size*4;
        }
    }
    return err;
}

int32_t f_bf_mem_rd(t_l502_hnd hnd, uint32_t addr, uint32_t* regs, uint32_t size)
{
    int err = 0;

    if (!err)
        err = f_bf_wait_cmd_done(hnd);

    while (!err && size)
    {
        int i;
        int get_size = (size < L502_BF_REQ_DATA_SIZE_MAX) ? size :
                                                       L502_BF_REQ_DATA_SIZE_MAX;

        /* записываем переметры передачи - размер и адрес в памяти BlackFin */
        if (!err)
            err = _fpga_reg_write(hnd,  L502_REGS_BF_REQ_SIZE, get_size);
        if (!err)
            err = _fpga_reg_write(hnd, L502_REGS_BF_REQ_ADDR, addr);
        /* даем команду на запис */
        if (!err)
            err = _fpga_reg_write(hnd, L502_REGS_BF_CMD, L502_BF_CMD_READ);

        /* ждем, пока операция не будет завершена */
        if (!err)
            err = f_bf_wait_cmd_done(hnd);

        /* записываем блок данных в буфер ПЛИС */
        for (i=0; (i < get_size) && !err; i++)
        {
            err = _fpga_reg_read(hnd, L502_REGS_BF_REQ_DATA+i, &regs[i]);
        }

        if (!err)
        {
            size -= get_size;
            regs += get_size;
            addr += get_size*4;
        }
    }
    return err;
}





static int32_t f_check_bf_firm(t_l502_hnd hnd)
{


    int32_t err = _fpga_reg_write(hnd, L502_REGS_BF_CMD, L502_BF_CMD_HDMA_RST);
    if (!err)
    {
        uint32_t rcv_wrds[2], recvd;

        /* Проверяем версию прошивки BlackFin */
        err = L502_BfExecCmd(hnd, L502_BF_CMD_CODE_GET_PARAM, L502_BF_PARAM_FIRM_VERSION,
                   NULL, 0, rcv_wrds, 2, L502_BF_CMD_DEFAULT_TOUT, &recvd);
        if (!err)
        {
            if (recvd >= 1)
            {
                hnd->bf_ver = rcv_wrds[0];
            }
            else
            {
                err = L502_ERR_BF_CMD_RETURN_INSUF_DATA;
            }

            hnd->bf_features = recvd >= 2 ? rcv_wrds[1] : 0;
        }
    }

    /* Проверка состояния прошивки (запущен поток сбора или нет) */
    if (!err)
    {
        uint32_t mode, streams;

        err = _get_bf_par(hnd, L502_BF_PARAM_STREAM_MODE, &mode, 1);
        if (!err)
            err = _get_bf_par(hnd, L502_BF_PARAM_ENABLED_STREAMS, &streams, 1);
        if (!err)
        {
            err = osspec_mutex_lock(hnd->mutex_cfg, L502_CFG_LOCK_MUTEX_TOUT);
            if (!err)
            {
                if (mode==L502_BF_MODE_IDLE)
                {
                    hnd->flags &= ~_FLAGS_STREAM_RUN;
                }
                else
                {
                    hnd->flags |= _FLAGS_STREAM_RUN;
                }
                hnd->streams = streams;
                osspec_mutex_release(hnd->mutex_cfg);
            }
        }
    }


    /* передаем информацию о модуле */
    if (!err && !(hnd->flags & _FLAGS_STREAM_RUN))
    {
        uint32_t put_wrds[3];
        uint32_t ch;
        put_wrds[0] = hnd->info.devflags;
        put_wrds[1] = hnd->info.fpga_ver | ((uint32_t)hnd->info.plda_ver<<16);
        err = _set_bf_par(hnd, L502_BF_PARAM_MODULE_INFO, put_wrds, 2);

        for (ch=0; !err && (ch < L502_DAC_CH_CNT); ch++)
        {
            float* pk = (float*)&put_wrds[1];
            float* po = (float*)&put_wrds[2];
            put_wrds[0] = ch;
            *pk = (float)hnd->info.cbr.dac[ch].k;
            *po = (float)hnd->info.cbr.dac[ch].offs;
            err = _set_bf_par(hnd, L502_BF_PARAM_DAC_COEF, put_wrds, 3);
        }
    }




    if (!err && (hnd->mode!=L502_MODE_DSP))
        err = L502_SetMode(hnd, L502_MODE_DSP);

    return err;
}




LPCIE_EXPORT(int32_t) L502_BfCheckFirmwareIsLoaded(t_l502_hnd hnd, uint32_t *version)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err)
    {
        err = f_check_bf_firm(hnd);
        if (!err && version)
            *version = hnd->bf_ver;
    }
    return err;
}

LPCIE_EXPORT(int32_t) L502_BfLoadFirmware(t_l502_hnd hnd, const char* filename)
{
    uint32_t* ldr_buff = NULL;
    FILE* ldr_file=NULL;


    int32_t err = L502_CHECK_HND(hnd);
    int next_err = 0;

    if (!err && !(hnd->info.devflags & L502_DEVFLAGS_BF_PRESENT))
        err = L502_ERR_BF_NOT_PRESENT;


    if (!err && (filename!=NULL))
    {
        ldr_file = fopen(filename, "rb");
        if (ldr_file==NULL)
            err = L502_ERR_LDR_FILE_OPEN;
    }

    if (!err)
    {
        ldr_buff = malloc(LDR_BUFF_SIZE);
        if (ldr_buff == NULL)
            err = L502_ERR_MEMORY_ALLOC;
    }

    if (!err)
    {
        err = osspec_mutex_lock(hnd->mutex_bf, L502_BF_LOCK_MUTEX_TOUT);
        if (!err)
        {
            int rd_size = 0;
            int stop = 0;
            uint32_t reg;
            uint8_t hdr[BF_LDR_HDR_SIZE];
            t_timer tmr;

            //uint32_t* pdw = (uint32_t*)ldr_buff;
            t_bf_ldr_pkt pkt, pkt_next;
            uint32_t bf_val = 0;

            _fpga_reg_read(hnd, L502_REGS_BF_CTL, &bf_val);
            _fpga_reg_write(hnd, L502_REGS_BF_CTL, L502_REGBIT_BF_CTL_DSP_MODE_Msk
                            | (bf_val & 0xF00)); //set rst
            SLEEP_MS(1);
            _fpga_reg_write(hnd, L502_REGS_BF_CTL, L502_REGBIT_BF_CTL_DSP_MODE_Msk |
                            L502_REGBIT_BF_CTL_BF_RESET_Msk | (bf_val & 0xF00));  //release rst

            timer_set(&tmr, L502_BF_WAIT_LOAD_RDY_TOUT*CLOCK_CONF_SECOND/1000);
            do
            {
                _fpga_reg_read(hnd, L502_REGS_BF_CTL, &reg);
                if ((reg&L502_REGBIT_BF_CTL_HOST_WAIT_Msk) && timer_expired(&tmr))
                    err = L502_ERR_BF_LOAD_RDY_TOUT;
            } while (!err && (reg&L502_REGBIT_BF_CTL_HOST_WAIT_Msk));

            if (!err)
            {
                err = fread(hdr, 1, BF_LDR_HDR_SIZE, ldr_file) == BF_LDR_HDR_SIZE ?
                      f_parse_ldr_hdr(hdr, &pkt) : L502_ERR_LDR_FILE_READ;
            }

            while (!err && !stop)
            {
                if (next_err)
                    err = next_err;
                else if (((pkt.flags&BF_LDR_FLAG_FILL) == 0) && (pkt.size!=0))
                {
                    int r_size = (pkt.size > LDR_BUFF_SIZE) ? LDR_BUFF_SIZE : pkt.size;

                    rd_size = (int)fread(ldr_buff, 1, r_size, ldr_file);
                    if (rd_size!=r_size)
                        err = L502_ERR_LDR_FILE_READ;
                }
                if (!err)
                {
                    if (pkt.size > LDR_BUFF_SIZE)
                    {
                        pkt_next = pkt;
                        pkt_next.addr += LDR_BUFF_SIZE;
                        pkt_next.size -= LDR_BUFF_SIZE;
                        pkt.size = LDR_BUFF_SIZE;
                    }
                    else
                    {
                        next_err = fread(hdr, 1, BF_LDR_HDR_SIZE, ldr_file) == BF_LDR_HDR_SIZE ?
                                  f_parse_ldr_hdr(hdr, &pkt_next) : L502_ERR_LDR_FILE_READ;
                        if (next_err)
                        {
                            pkt_next.size = 0;
                        }
                    }

                    if (pkt.size!=0)
                    {
                        uint32_t size = ((pkt.size+31)/(32))*8;
                        if (pkt.flags & BF_LDR_FLAG_FILL)
                        {
                            uint32_t i;
                            for (i=0; i < size; i++)
                                ldr_buff[i] = pkt.arg;
                        }

                        if ((pkt.flags & BF_LDR_FLAG_FINAL)
                            || ((pkt_next.flags & BF_LDR_FLAG_FINAL) && (pkt_next.size==0)))
                        {
                            uint32_t buf_pos = 0;
                            err = BF_CHECK_ADDR_SIZE(pkt.addr, size);

                            if (!err && (size > 8))
                            {
                                err = f_bf_mem_wr(hnd, pkt.addr, ldr_buff, size-8);
                                pkt.addr+=4*(size-8);
                                size = 8;
                                buf_pos = size-8;
                            }

                            if (!err)
                                 err = _fpga_reg_write(hnd, L502_REGS_BF_CMD, L502_BF_CMD_HIRQ);
                            if (!err)
                                err = f_bf_mem_wr(hnd, pkt.addr, &ldr_buff[buf_pos], size);
                            stop=1;

                            if (!err)
                            {
                                err = _fpga_reg_write(hnd, L502_REGS_BF_CTL, L502_REGBIT_BF_CTL_DSP_MODE_Msk |
                                            L502_REGBIT_BF_CTL_BF_RESET_Msk);
                            }


                            /*L583_WriteDwordFPGA(hnd, L583_FPGA_ADDR_BFOFFS, L583_BF_ADDR_ENDPROG);
                            L583_WriteArrBF(hnd, L583_BF_ADDR_ENDPROG, 1, &nl);
                            L583_WriteArrBF(hnd, pkt.addr + (size-8)*4, 8, &pdw[size-8]);*/

                            //L583_WriteDwordFPGA(hnd, L583_FPGA_ADDR_BFCTRL, 0x1); //set fast
                        }
                        else if (!(pkt.flags & BF_LDR_FLAG_IGNORE))
                        {
                            err = BF_CHECK_ADDR_SIZE(pkt.addr, size);
                            if (!err)
                                err = f_bf_mem_wr(hnd, pkt.addr, ldr_buff, size);
                        }
                    }
                    pkt = pkt_next;
                }
            }
            osspec_mutex_release(hnd->mutex_bf);
        }
    }



    if (ldr_file!=NULL)
        fclose(ldr_file);
    free(ldr_buff);





    if (!err)
    {        
        SLEEP_MS(100);
        err = f_check_bf_firm(hnd);
    }


    return err;
}




LPCIE_EXPORT(int32_t) L502_BfMemWrite(t_l502_hnd hnd, uint32_t addr, const uint32_t* regs, uint32_t size)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err && !(hnd->info.devflags & L502_DEVFLAGS_BF_PRESENT))
        err = L502_ERR_BF_NOT_PRESENT;;
    if (!err)
        err = BF_CHECK_ADDR_SIZE(addr,size);
    if (!err)
        err = osspec_mutex_lock(hnd->mutex_bf, L502_BF_LOCK_MUTEX_TOUT);
    if (!err)
    {
        int32_t release_err;
        err = f_bf_mem_wr(hnd, addr, regs, size);
        release_err = osspec_mutex_release(hnd->mutex_bf);
        if (!err)
            err = release_err;
    }
    return err;
}

//int l502_eeprom_read(t_l502_hnd, uint32_t addr, uint)

LPCIE_EXPORT(int32_t) L502_BfMemRead(t_l502_hnd hnd, uint32_t addr, uint32_t* regs, uint32_t size)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err && !(hnd->info.devflags & L502_DEVFLAGS_BF_PRESENT))
        err = L502_ERR_BF_NOT_PRESENT;
    if (!err)
        err = BF_CHECK_ADDR_SIZE(addr,size);
    if (!err)
        err = osspec_mutex_lock(hnd->mutex_bf, L502_BF_LOCK_MUTEX_TOUT);
    if (!err)
    {
        int32_t release_err;
        err = f_bf_mem_rd(hnd, addr, regs, size);
        release_err = osspec_mutex_release(hnd->mutex_bf);
        if (!err)
            err = release_err;
    }
    return err;
}

int l502_bf_start_cmd(t_l502_hnd hnd, uint16_t cmd_code, uint32_t par,
                      const uint32_t* data, uint32_t size)
{
    int err = L502_CHECK_HND(hnd);
    if (!err && !(hnd->info.devflags & L502_DEVFLAGS_BF_PRESENT))
        err = L502_ERR_BF_NOT_PRESENT;
    if (size > L502_BF_CMD_DATA_SIZE_MAX)
        err = L502_ERR_BF_INVALID_CMD_DATA_SIZE;
    if (!err)
        err = osspec_mutex_lock(hnd->mutex_bf, L502_BF_LOCK_MUTEX_TOUT);

    if (!err)
    {
        t_l502_bf_cmd cmd;
        cmd.code = cmd_code;
        cmd.data_size = size;
        cmd.param = par;
        cmd.result = 0;
        cmd.status = L502_BF_CMD_STATUS_REQ;



        if (size)
        {
            memcpy(cmd.data, data, size*sizeof(data[0]));
        }

        /* если размер больше, чем можем записать за раз, то сперва записываем
         * хвост, чтобы не получилось, что информация о команде будет записана
         * раньше данных */
        if (size > BF_CMD_FIRST_DATA_BLOCK_SIZE)
        {
            err = f_bf_mem_wr(hnd, L502_BF_MEMADDR_CMD+L502_BF_REQ_DATA_SIZE_MAX*4,
                              &cmd.data[BF_CMD_FIRST_DATA_BLOCK_SIZE],
                              size - BF_CMD_FIRST_DATA_BLOCK_SIZE);
            size = BF_CMD_FIRST_DATA_BLOCK_SIZE;
        }
        if (!err)
        {
            err = f_bf_mem_wr(hnd, L502_BF_MEMADDR_CMD, (const uint32_t*)&cmd,
                            (offsetof(t_l502_bf_cmd, data))/4 + size);
        }

        if (err)
        {
            osspec_mutex_release(hnd->mutex_bf);
        }
    }
    return err;
}

int l502_bf_cmd_check(t_l502_hnd hnd, int32_t *res)
{
    t_l502_bf_cmd cmd;
    int err = f_bf_mem_rd(hnd, L502_BF_MEMADDR_CMD, (uint32_t*)&cmd,
                           L502_BF_REQ_DATA_SIZE_MIN);
    if (!err)
    {
        if (cmd.status != L502_BF_CMD_STATUS_DONE)
            err = L502_ERR_BF_CMD_IN_PROGRESS;
        else if (res)
        {
            *res = cmd.result;            
        }
    }

    if (err!=L502_ERR_BF_CMD_IN_PROGRESS)
        osspec_mutex_release(hnd->mutex_bf);

    return err;
}

LPCIE_EXPORT(int32_t) L502_BfExecCmd(t_l502_hnd hnd, uint16_t cmd_code, uint32_t par,
                        const uint32_t* snd_data, uint32_t snd_size,
                        uint32_t* rcv_data, uint32_t rcv_size, uint32_t tout,
                        uint32_t* recvd_size)
{
    t_l502_bf_cmd cmd;
    int32_t err = l502_bf_start_cmd(hnd, cmd_code, par, snd_data, snd_size);
    int done = 0;
    t_timer tmr;

    timer_set(&tmr, tout*CLOCK_CONF_SECOND/1000);
    while (!err && !done)
    {
        err = l502_bf_cmd_check(hnd, &cmd.result);
        if (!err)
        {
            done = 1;
            if (rcv_size != 0)
            {
                err = L502_BfMemRead(hnd, L502_BF_MEMADDR_CMD, (uint32_t*)&cmd,
                                   (offsetof(t_l502_bf_cmd, data))/4 + rcv_size);
                memcpy(rcv_data, cmd.data, rcv_size*sizeof(rcv_data[0]));
                if (recvd_size!=NULL)
                    *recvd_size = cmd.data_size;
            }
            if (!err)
                err = cmd.result;
        }
        else if (err==L502_ERR_BF_CMD_IN_PROGRESS)
        {
            if (timer_expired(&tmr))
            {
                err = L502_ERR_BF_CMD_TIMEOUT;
                 osspec_mutex_release(hnd->mutex_bf);
            }
            else
            {
                err = 0;
                SLEEP_MS(20);
            }
        }
    }
    return err;
}


#ifndef OS_SPEC_CFG_H_
#define OS_SPEC_CFG_H_

#include "l502api.h"


#define OSSPEC_USE_MUTEX

#define OS_SPEC_ERR_MUTEX_INVALID_HANDLE L502_ERR_MUTEX_INVALID_HANDLE
#define OS_SPEC_ERR_MUTEX_LOCK_TOUT      L502_ERR_MUTEX_LOCK_TOUT
#define OS_SPEC_ERR_MUTEX_RELEASE        L502_ERR_MUTEX_RELEASE





#endif

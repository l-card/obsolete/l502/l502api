/***************************************************************************//**
  @file l502_eeprom.c
  @author Borisov Alexey <borisov@lcard.ru>
  @date   14.03.2011
  Файл содержит функции для работы с EEPROM модуля
 ******************************************************************************/
#include "l502_private.h"
#include <string.h>
#include <stdlib.h>



/* разрешение/запрещение потоков ввода вывода в соответствии с полем hnd->streams */
static int32_t f_set_streams(t_l502_hnd hnd, uint32_t streams)
{
    int32_t err = 0;
    if (hnd->mode == L502_MODE_FPGA)
    {
        err = _fpga_reg_write(hnd, L502_REGS_IOARITH_IN_STREAM_ENABLE,
                          (streams & L502_STREAM_ADC ? 0x01 : 0) |
                          (streams & L502_STREAM_DIN ? 0x02 : 0));
    }

    if (!err)
    {
        hnd->streams = streams;
    }
    return err;
}





/* Функция автоматического рассчета параметов DMA для входного потока
   на основе частоты сбора данных */
static int32_t f_set_stream_in_dma_par(t_l502* hnd)
{
    int32_t err = 0;


    t_lpcie_stream_ch_params params;
    double din_freq = 0;
    uint32_t total_size = 0;
    int ref_freq = hnd->set.ref_freq;


    memset(&params, 0, sizeof(params));
    params.ch = L502_DMA_CHNUM_IN;

    /* рассчитываем частоту сбора для потоков АЦП и DIN */
    if (hnd->streams & L502_STREAM_ADC)
    {
        double f_frame;
        L502_GetAdcFreq(hnd, NULL, &f_frame);
        din_freq = f_frame*hnd->set.lch_cnt;
    }

    if (hnd->streams & L502_STREAM_DIN)
    {
        din_freq+=ref_freq/hnd->set.din_freq_div;
    }

    /* размер полного буфера определяем таким, чтобы его хватило на
       L502_DMA_IN_BUF_FOR_SEC секунд постоянного сбора, но не меньше минимального
       размера */
    total_size = (uint32_t)(L502_DMA_IN_BUF_FOR_SEC*din_freq);
    if (total_size<L502_DMA_IN_BUF_SIZE_MIN)
        total_size=L502_DMA_IN_BUF_SIZE_MIN;






    /* рассчитываем IRQ_STEP, чтобы он был  L502_DMA_IN_MAX_IRQ_PER_SEC
       в секунду */

    params.irq_step = hnd->dma_params[L502_DMA_CHNUM_IN].irq_step ?
                hnd->dma_params[L502_DMA_CHNUM_IN].irq_step :
                (din_freq>L502_DMA_IN_MAX_IRQ_PER_SEC) ?
                (uint32_t)(din_freq/L502_DMA_IN_MAX_IRQ_PER_SEC) : 1;

    /* для эффиктивности делаем размер буфера кратным irq_step */
    total_size = ((total_size+params.irq_step-1)/params.irq_step)*params.irq_step;

    params.buf_size = hnd->dma_params[L502_DMA_CHNUM_IN].buf_size ?
                hnd->dma_params[L502_DMA_CHNUM_IN].buf_size : total_size;

    err = _stream_set_params(hnd, &params);

    if (!err && (hnd->mode == L502_MODE_DSP))
    {
        /* для BlackFin нужно так же установить еще шаг прерываний для
           приема данных от SPORT'а */
        uint32_t size;
        err = _get_bf_par(hnd, L502_BF_PARAM_IN_BUF_SIZE, &size, 1);
        if (!err)
        {

            uint32_t instep = params.irq_step;
            if (instep>size/4)
            {
                instep=size/4;
            }
            if (instep > 0x8000)
                instep = 0x8000;
            err = _set_bf_par(hnd, L502_BF_PARAM_IN_STEP_SIZE, &instep, 1);
        }
    }
    return err;
}



/* Функция автоматического рассчета параметов DMA для входного потока
   на основе частоты сбора данных */
static int32_t f_set_stream_out_dma_par(t_l502* hnd)
{
    int32_t err = 0;
    t_lpcie_stream_ch_params params;

    memset(&params, 0, sizeof(params));
    params.ch = L502_DMA_CHNUM_OUT;

    /* разбиваем его на страницы, пробуем на дефолтный размер, если
       страниц получится слишком много => увеличиваем размер страницы */
    params.buf_size = hnd->dma_params[L502_DMA_CHNUM_OUT].buf_size ?
                hnd->dma_params[L502_DMA_CHNUM_OUT].buf_size :
                L502_DMA_OUT_BUF_SIZE;

    params.irq_step = hnd->dma_params[L502_DMA_CHNUM_OUT].irq_step ?
                hnd->dma_params[L502_DMA_CHNUM_OUT].irq_step :
                L502_DMA_OUT_IRQ_STEP;

    err = _stream_set_params(hnd, &params);

    return err;
}


static int32_t f_out_stream_preload(t_l502_hnd hnd)
{
    int32_t err = f_set_stream_out_dma_par(hnd);
    if (!err)
        err = _stream_start(hnd, L502_DMA_CHNUM_OUT, 0);

    if (!err && (hnd->mode == L502_MODE_DSP))
    {
        err = L502_BfExecCmd(hnd, L502_BF_CMD_CODE_PRELOAD, 0, NULL, 0,
                             NULL, 0, L502_BF_REQ_TOUT, NULL);
    }

    if (!err)
    {
        hnd->flags |= _FLAGS_PRELOAD_DONE;
    }
    return err;
}



LPCIE_EXPORT(int32_t) L502_StreamsEnable(t_l502_hnd hnd, uint32_t streams)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err)
        err = osspec_mutex_lock(hnd->mutex_cfg, L502_CFG_LOCK_MUTEX_TOUT);
    if (!err)
    {
        if (hnd->mode == L502_MODE_DSP)
        {
            err = L502_BfExecCmd(hnd, L502_BF_CMD_CODE_STREAM_EN, streams,
                                 NULL, 0, NULL, 0, L502_BF_REQ_TOUT, NULL);
            if (!err)
                hnd->streams |= streams;
        }
        else
        {
            uint32_t old_streams = hnd->streams;
            err = f_set_streams(hnd, hnd->streams | streams);

            if (hnd->flags & _FLAGS_STREAM_RUN)
            {
                /* если не было разрешено потока на ввод до этого,
                   а при вызове стал разрешен => инициализируем его */
                if (!err && !(old_streams & L502_STREAM_ALL_IN) &&
                        (streams & L502_STREAM_ALL_IN))
                {
                    err = f_set_stream_in_dma_par(hnd);
                    if (!err)
                        err = _stream_start(hnd, L502_DMA_CHNUM_IN, 0);
                }
                else if (!err && ((old_streams & L502_STREAM_ALL_IN) !=
                                  (streams & L502_STREAM_ALL_IN)))
                {
                    err = f_set_stream_in_dma_par(hnd);
                }
            }
        }
        osspec_mutex_release(hnd->mutex_cfg);
    }
    return err;
}

LPCIE_EXPORT(int32_t) L502_StreamsDisable(t_l502_hnd hnd, uint32_t streams)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err)
        err = osspec_mutex_lock(hnd->mutex_cfg, L502_CFG_LOCK_MUTEX_TOUT);
    if (!err)
    {
        if (hnd->mode == L502_MODE_DSP)
        {

            err = L502_BfExecCmd(hnd, L502_BF_CMD_CODE_STREAM_DIS, streams,
                                 NULL, 0, NULL, 0, L502_BF_REQ_TOUT, NULL);
            if (!err)            
                hnd->streams &= ~streams;

        }
        else
        {
            uint32_t old_streams = hnd->streams;
            err = f_set_streams(hnd, hnd->streams & ~streams);

            if (hnd->flags & _FLAGS_STREAM_RUN)
            {
                /* если все потоки на ввод были запрещены, то
                   останавливаем их */
                if (!err&& (old_streams & L502_STREAM_ALL_IN) &&
                        !(hnd->streams & L502_STREAM_ALL_IN))
                    err = _stream_stop(hnd, L502_DMA_CHNUM_IN);
                if (!err&& (old_streams & L502_STREAM_ALL_OUT) &&
                        !(hnd->streams & L502_STREAM_ALL_OUT))
                {
                    err = _stream_stop(hnd, L502_DMA_CHNUM_OUT);
                    if (!err)
                    {
                        hnd->flags &= ~_FLAGS_PRELOAD_DONE;
                    }
                }
            }
        }

        osspec_mutex_release(hnd->mutex_cfg);
    }
    return err;
}

LPCIE_EXPORT(int32_t) L502_StreamsStart(t_l502_hnd hnd)
{
    int err = L502_CHECK_HND(hnd);
    if (!err && (hnd->flags & _FLAGS_STREAM_RUN))
        err = L502_ERR_STREAM_IS_RUNNING;
    if (!err && (hnd->mode==L502_MODE_FPGA))
    {
        uint32_t reg;
        err = _fpga_reg_read(hnd, L502_REGS_IOHARD_IO_MODE, &reg);
        if (!err && !(reg & L502_REGBIT_ADC_SLV_CLK_LOCK_Msk))
            err = L502_ERR_REF_FREQ_NOT_LOCKED;
    }


    if (!err)
        err = osspec_mutex_lock(hnd->mutex_cfg, L502_CFG_LOCK_MUTEX_TOUT);

    if (!err)
    {
        int in_started = 0;
        f_set_streams(hnd, hnd->streams);



        /* выполняем предзагрузку первого слова выходного потока и
           коммутатора АЦП (при наличии DSP это делает DSP) */
        if (!err && (hnd->mode==L502_MODE_FPGA))
        {
            /* предзагрузку значения на вывод делаем только если реально данные уже были
             * предзагружены в буфер платы */
            if ((hnd->streams & L502_STREAM_ALL_OUT) &&
                    (hnd->flags & (_FLAGS_PRELOAD_DONE | _FLGAS_CYCLE_MODE)))
            {
                err = _fpga_reg_write(hnd, L502_REGS_IOHARD_OUTSWAP_BFCTL, 1);
                //if (!err)
                //    err = _fpga_reg_write(hnd, L502_REGS_IOHARD_DAC_DIGOUT_SWAP, 1);
                //if (!err)
                //    err = _fpga_reg_write(hnd, L502_REGS_IOHARD_DAC_DIGOUT_SWAP, 1);
            }


            /* предзагрузку АЦП должны делать всегда, так как по этой функции
             * выполняется часть инициализации параметров синхронного сбора! */
            if (!err)
            {
                err = _fpga_reg_write(hnd, L502_REGS_IOHARD_PRELOAD_ADC, 1);
            }


            SLEEP_MS(20);
        }


        /* запуск потока на ввод выполняем при запуске синхронного ввода-вывода */
        if (!err && (hnd->streams & L502_STREAM_ALL_IN))
        {
            err = f_set_stream_in_dma_par(hnd);
            if (!err)
                err = _stream_start(hnd, L502_DMA_CHNUM_IN, 0);
            if (!err)
                in_started = 1;
        }

        if (!err)
        {
            if (hnd->mode == L502_MODE_FPGA)
            {
                /* взводим сигнал GO, указывающий что запущен синхронных ввод-вывод */
                err = _fpga_reg_write(hnd, L502_REGS_IOHARD_GO_SYNC_IO, 1);
            }
            else if (hnd->mode == L502_MODE_DSP)
            {
                err = L502_BfExecCmd(hnd, L502_BF_CMD_CODE_STREAM_START, 0,
                                  NULL, 0, NULL, 0, L502_BF_CMD_DEFAULT_TOUT, NULL);
            }
        }

        if (err && in_started)
            err = _stream_free(hnd, L502_DMA_CHNUM_IN);


        if (!err)
        {
            hnd->flags |= _FLAGS_STREAM_RUN;
            hnd->proc_adc_ch = 0;
        }

        osspec_mutex_release(hnd->mutex_cfg);
    }
    return err;
}



LPCIE_EXPORT(int32_t) L502_StreamsStop(t_l502_hnd hnd)
{
    int err = L502_CHECK_HND(hnd);
    if (!err)
        err = osspec_mutex_lock(hnd->mutex_cfg, L502_CFG_LOCK_MUTEX_TOUT);
    if (!err)
    {
        if (hnd->mode==L502_MODE_FPGA)
        {
            err = _fpga_reg_write(hnd, L502_REGS_IOHARD_GO_SYNC_IO, 0);
        }
        else if (hnd->mode == L502_MODE_DSP)
        {
            err = L502_BfExecCmd(hnd, L502_BF_CMD_CODE_STREAM_STOP, 0,
                              NULL, 0, NULL, 0, L502_BF_CMD_DEFAULT_TOUT, NULL);
        }

        if (!err) // && (hnd->streams & L502_STREAM_ALL_IN))
            err = _stream_free(hnd, L502_DMA_CHNUM_IN);
        if (!err)
            err = _stream_free(hnd, L502_DMA_CHNUM_OUT);

        hnd->flags &= ~(_FLAGS_STREAM_RUN | _FLAGS_PRELOAD_DONE);

        osspec_mutex_release(hnd->mutex_cfg);
    }
    return err;
}

LPCIE_EXPORT(int32_t) L502_IsRunning(t_l502_hnd hnd)
{
    int err = L502_CHECK_HND(hnd);
    uint32_t bf_mode;
    if (!err && (hnd->mode==L502_MODE_DSP))
    {
        err = _get_bf_par(hnd, L502_BF_PARAM_STREAM_MODE, &bf_mode, 1);
    }

    if (!err)
        err = osspec_mutex_lock(hnd->mutex_cfg, L502_CFG_LOCK_MUTEX_TOUT);
    if (!err)
    {
        if (hnd->mode==L502_MODE_DSP)
        {
            if (bf_mode==L502_BF_MODE_IDLE)
            {
                hnd->flags &= ~_FLAGS_STREAM_RUN;
            }
            else
            {
                hnd->flags |= _FLAGS_STREAM_RUN;
            }
        }

        if (!(hnd->flags & _FLAGS_STREAM_RUN))
            err = L502_ERR_STREAM_IS_NOT_RUNNING;

        osspec_mutex_release(hnd->mutex_cfg);
    }
    return err;
}



LPCIE_EXPORT(int32_t) L502_Recv(t_l502_hnd hnd, uint32_t* buf, uint32_t size, uint32_t tout)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err && (buf==NULL))
        err = L502_ERR_INVALID_POINTER;
    if (!err)
    {
        err = _stream_in_read(hnd, buf, size, tout);
    }
    return err;
}

LPCIE_EXPORT(int32_t) L502_Send(t_l502_hnd hnd, const uint32_t* buf, uint32_t size, uint32_t tout)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err && (buf==NULL))
        err = L502_ERR_INVALID_POINTER;

    /* если разрешен синхронный вывод, но не было
     * вызова L502_PreloadStart() или не был установлен синхронных режим, то
     * делаем запуск потока вывода при первой записи */
    if (!err && (hnd->streams & L502_STREAM_ALL_OUT))
    {
        if (!err)
            err = osspec_mutex_lock(hnd->mutex_cfg, L502_CFG_LOCK_MUTEX_TOUT);
        if (!err)
        {
            if (!(hnd->flags & (_FLAGS_PRELOAD_DONE | _FLGAS_CYCLE_MODE)))
            {
                err = f_out_stream_preload(hnd);
            }
            osspec_mutex_release(hnd->mutex_cfg);
        }
    }

    if (!err)
    {
        err = _stream_in_write(hnd, buf, size, tout);
    }
    return err;
}


LPCIE_EXPORT(int32_t) L502_PreloadStart(t_l502_hnd hnd)
{
    int err = L502_CHECK_HND(hnd);
    if (!err)
        err = osspec_mutex_lock(hnd->mutex_cfg, L502_CFG_LOCK_MUTEX_TOUT);
    if (!err)
    {
        err = f_out_stream_preload(hnd);
        osspec_mutex_release(hnd->mutex_cfg);
    }
    return err;
}


LPCIE_EXPORT(int32_t) L502_AsyncGetAdcFrame(t_l502_hnd hnd, uint32_t flags, uint32_t tout, double* data)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err & (data==NULL))
        err = L502_ERR_INVALID_POINTER;
    if (!err)
        err = osspec_mutex_lock(hnd->mutex_cfg, L502_CFG_LOCK_MUTEX_TOUT);
    if (!err)
    {
        if (hnd->mode == L502_MODE_FPGA)
        {
            /* если запущен хоть один поток на ввод синхронно, то ввод кадра
             * не возможне */
            if ((hnd->flags & _FLAGS_STREAM_RUN) && (hnd->streams & L502_STREAM_ALL_IN))
            {
                err = L502_ERR_STREAM_IS_RUNNING;
            }

            if (!err)
            {
                int need_stop = 0;
                int old_streams = hnd->streams;
                uint32_t* wrds = NULL;
                int32_t rcv_size = hnd->set.lch_cnt;

                /* разрешаем поток для АЦП */
                err = f_set_streams(hnd, (hnd->streams & ~L502_STREAM_ALL_IN)
                                    | L502_STREAM_ADC);

                hnd->proc_adc_ch = 0;



                if (!err)
                {
                     /* инициализируем буфер для приема - достаточно всего на один кадр */
                    t_lpcie_stream_ch_params par = hnd->dma_params[L502_DMA_CH_IN];
                    hnd->dma_params[L502_DMA_CH_IN].buf_size = L502_DMA_IN_BUF_SIZE_MIN;
                    hnd->dma_params[L502_DMA_CH_IN].irq_step = hnd->set.lch_cnt;
                    err = f_set_stream_in_dma_par(hnd);

                    /* восстанавливаем параметры в структуре, что были заданы
                      до этой функции */
                    hnd->dma_params[L502_DMA_CH_IN] = par;
                }

                if (!err)
                {
                    /* выделяем массив для необработанных отсчетов */
                    wrds = malloc(sizeof(wrds[0])*rcv_size);
                    if (wrds==NULL)
                        err = L502_ERR_MEMORY_ALLOC;
                }

                /* предзагрузка логической таблицы для АЦП */
                if (!err)
                    err = _fpga_reg_write(hnd, L502_REGS_IOHARD_PRELOAD_ADC, 1);

                /* запуск канала DMA на прием данных */
                if (!err)
                    err = _stream_start(hnd, L502_DMA_CHNUM_IN, 1);

                /* если общий синхронный ввод не был запущен  - разрешаем его */
                if (!err)
                {
                    if (!(hnd->flags & _FLAGS_STREAM_RUN))
                    {
                        err = _fpga_reg_write(hnd, L502_REGS_IOHARD_GO_SYNC_IO, 1);
                        if (!err)
                            need_stop = 1;
                    }
                }

                if (!err)
                {
                    /* принимаем отсчеты от одного кадра */
                    int32_t rcv = L502_Recv(hnd, wrds, rcv_size, tout);
                    if (rcv < 0)
                        err = rcv;
                    else if (rcv!=rcv_size)
                        err = L502_ERR_RECV_INSUFFICIENT_WORDS;
                    else
                    {
                        err = L502_ProcessAdcData(hnd, wrds, data, (uint32_t*)&rcv_size, flags);
                    }
                }

                /* если в этой функции запустили синхронный сбор, то останвливаем его */
                if (need_stop)
                    _fpga_reg_write(hnd, L502_REGS_IOHARD_GO_SYNC_IO, 0);

                hnd->proc_adc_ch = 0;
                _stream_free(hnd, L502_DMA_CHNUM_IN);
                /* восстанавливаем те потоки, которые были разрешены */
                f_set_streams(hnd, old_streams);
                free(wrds);
            }
        }
        else if (hnd->mode == L502_MODE_DSP)
        {
            err = L502_BfExecCmd(hnd, L502_BF_CMD_CODE_ADC_GET_FRAME,
                                 0, NULL, 0, (uint32_t*)data, 2*hnd->set.lch_cnt,
                                 L502_BF_CMD_DEFAULT_TOUT, NULL);
        }
        else
        {
            err = L502_ERR_INVALID_MODE;
        }

        osspec_mutex_release(hnd->mutex_cfg);
    }
    return err;
}




LPCIE_EXPORT(int32_t) L502_OutCycleLoadStart(t_l502_hnd hnd, uint32_t size)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err)
    {

        err = osspec_mutex_lock(hnd->mutex_cfg, L502_CFG_LOCK_MUTEX_TOUT);
        if (!err)
        {
            /** @todo проверить правильность момента вызова */
            err = _cycle_load_start(hnd, L502_DMA_CHNUM_OUT, size);
            if (!err)
                hnd->flags |= _FLGAS_CYCLE_MODE;

            osspec_mutex_release(hnd->mutex_cfg);
        }

    }
    return err;
}

LPCIE_EXPORT(int32_t) L502_OutCycleSetup(t_l502_hnd hnd, uint32_t flags)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err)
    {
        /** @todo проверить правильность момента вызова */
        err = _cycle_setup(hnd, L502_DMA_CHNUM_OUT, (flags & L502_OUT_CYCLE_FLAGS_FORCE) ?
                               LPCIE_CYCLE_SW_EVT_IMMIDIATLY : LPCIE_CYCLE_SW_EVT_END_OF_CYCLE);
    }
    return err;
}


LPCIE_EXPORT(int32_t) L502_OutCycleStop(t_l502_hnd hnd, uint32_t flags)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err)
    {
        /** @todo проверить правильность момента вызова */
        err = _cycle_stop(hnd, L502_DMA_CHNUM_OUT, (flags & L502_OUT_CYCLE_FLAGS_FORCE) ?
                              LPCIE_CYCLE_SW_EVT_IMMIDIATLY : LPCIE_CYCLE_SW_EVT_END_OF_CYCLE);
    }
    return err;
}

#ifndef L502_PRIVATE_H_
#define L502_PRIVATE_H_

#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif

#include "l502api.h"
#include "lpcie_ioctls.h"
#include "l502_fpga_regs.h"
#include "l502_spec.h"
#include "l502_bf_cmd_defs.h"

#include "osspec.h"





#define L502_SIGN 0xFAA10502

#define L502_CHECK_HND(hnd) ((hnd) ? (hnd)->sign == L502_SIGN ? L502_ERR_OK \
    : L502_ERR_INVALID_HANDLE : L502_ERR_INVALID_HANDLE)



/* на сколько секунд данных будет рассчитан внутренний буфер */
#define L502_DMA_IN_BUF_FOR_SEC  4
/* минимальный размер внутреннего буфера */
#define L502_DMA_IN_BUF_SIZE_MIN 16*1024
/* максимальное кол-во прерываний в секунду */
#define L502_DMA_IN_MAX_IRQ_PER_SEC  20

#define L502_DMA_OUT_BUF_SIZE  3*3*1024*1024
#define L502_DMA_OUT_IRQ_STEP  (3*1024*1024/64)


#define L502_BF_LOCK_MUTEX_TOUT  1000
#define L502_CFG_LOCK_MUTEX_TOUT 1000

#define L502_ALLOC_MAX_PAGES_CNT 32




/** структура, описывающая параметры логического канала */
typedef struct
{
    uint32_t ch; /** физический номер канала */
    uint32_t mode; /** режим работы канала из #t_l502_ch_mode */
    uint32_t range; /** диапазон измерения */
    uint32_t avg;  /** коэффициент усреднения */
} t_l502_lch;


typedef enum
{
    _FLAGS_OPENED             = 0x0001,
    _FLAGS_PRELOAD_DONE       = 0x0002,
    _FLGAS_CYCLE_MODE         = 0x0004,
    _FLAGS_STREAM_RUN         = 0x0080
} t_l502_state_flags;


typedef struct
{
    t_l502_lch lch[L502_LTABLE_MAX_CH_CNT];
    uint32_t lch_cnt;
    uint32_t adc_freq_div;
    uint32_t adc_frame_delay;
    uint32_t din_freq_div;
    uint32_t sync_mode;
    uint32_t sync_start_mode;
    uint32_t ref_freq;
    uint32_t dac_freq_div;
} t_l502_settings;



typedef struct st_l502
{
    uint32_t sign; /* признак описателя L502 */
    t_l502_state_flags flags; /* флаги состояния платы */
    t_l502_info info; /* информация о устройстве */
    t_l502_streams streams; /* какие синхронные потоки разрешены */
    t_l502_mode mode; /* режим работы (через ПЛИС или DSP) */
    uint32_t last_dout; /* последнее выданное значение на DIGOUT */

    t_mutex mutex_bf; /* мьютекс для доступа к DSP */
    t_mutex mutex_cfg; /* мьютекс для доступа к полям параметров и состояния модуля */

#ifdef _WIN32
    HANDLE file;
#else
    int file;
#endif

    t_lpcie_stream_ch_params dma_params[2];
    uint32_t proc_adc_ch; /* ожидаемый логический канал для следующей ProcessData() */
    t_l502_settings set; /* настройки платы */

    uint32_t bf_ver; /* версия прошивки BlackFin, если есть */
    uint32_t bf_features; /* дополниельные возможности, поддерживаемые прошивкой */
} t_l502;


int _check_eeprom(t_l502_hnd hnd);

#ifdef _WIN32
    #define SLEEP_MS(ms) Sleep(ms)
#else
    #define SLEEP_MS(ms) usleep(ms*1000)
#endif

int32_t _fpga_reg_write(t_l502_hnd hnd, uint32_t reg, uint32_t val);
int32_t _fpga_reg_read(t_l502_hnd hnd, uint32_t reg, uint32_t *val);
int32_t _open_by_list_intptr(t_l502_hnd hnd, void* intptr);
int32_t _free_intptr(void* intptr);
int32_t _dev_close(t_l502_hnd hnd);
int32_t _stream_start(t_l502_hnd hnd, uint32_t ch, int single);
int32_t _stream_stop(t_l502_hnd hnd, uint32_t ch);
int32_t _stream_free(t_l502_hnd hnd, uint32_t ch);
int32_t _stream_in_read(t_l502_hnd hnd, uint32_t* buff, int32_t size, uint32_t timeout);
int32_t _stream_in_write(t_l502_hnd hnd, const uint32_t* buff, int32_t size,
                         uint32_t timeout);
int32_t _stream_set_params(t_l502_hnd hnd, t_lpcie_stream_ch_params *par);
int32_t _stream_rdy_size(t_l502_hnd hnd, uint32_t ch, uint32_t *rdy_size);
int32_t _renew_info(t_l502_hnd hnd);
int32_t _get_drv_ver(t_l502_hnd hnd, uint32_t *ver);
int32_t _cycle_load_start(t_l502_hnd hnd, uint32_t ch, uint32_t size);
int32_t _cycle_setup(t_l502_hnd hnd, uint32_t ch, uint32_t evt);
int32_t _cycle_stop(t_l502_hnd hnd, uint32_t ch, uint32_t evt);

#define _set_bf_par(hnd, par, data, size) L502_BfExecCmd(hnd, L502_BF_CMD_CODE_SET_PARAM, \
    par, data, size, NULL, 0, L502_BF_CMD_DEFAULT_TOUT, NULL)

#define _get_bf_par(hnd, par, data, size) L502_BfExecCmd(hnd, L502_BF_CMD_CODE_GET_PARAM, \
    par, NULL, 0, data, size, L502_BF_CMD_DEFAULT_TOUT, NULL)



#endif

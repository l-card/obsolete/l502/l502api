/***************************************************************************//**
  @file l502_eeprom.h
  @author Borisov Alexey <borisov@lcard.ru>
  @date   14.03.2011

   Файл содержет описание констант и типов, для описание формата информации,
   записаной в EEPROM. Эти константы используются только внутри библиотеки и
   в специальном софте (для калибровки и обновлениия прошивки ПЛИС) и не
   доступны пользователю по-умолчанию
 ******************************************************************************/
#ifndef L502_EEPROM_H
#define L502_EEPROM_H

#include "l502api.h"



#define L502_EEPROM_CBR_SIGN   0x4C434352 //LCBR
#define L502_EEPROM_CBR_FROMAT 2

#define L502_EEPROM_CBR_SRC_ADC  1
#define L502_EEPROM_CBR_SRC_DAC  2


#define L502_EEPROM_SIZE        0x200000UL

#define L502_EEPROM_ADDR_DESCR  0x1F0000UL
#define L502_DESCR_MAX_SIZE     0x010000UL





#define L502_EEPROM_FORMAT      1
#define L502_DESCR_MIN_SIZE   (sizeof(t_l502_eeprom_hdr)+4)
#define L502_EEPROM_SIGN      0x4C524F4D


typedef struct
{
    uint32_t cbr_sign;
    uint32_t cbr_size;
    uint32_t format;
    uint32_t src;
    uint32_t flags;
    uint32_t reserv[3];
    uint64_t time;
    uint32_t channel_cnt;
    uint32_t range_cnt;        
} t_l502_eeprom_cbr_hdr;

typedef struct
{
    t_l502_eeprom_cbr_hdr hdr;
    t_l502_cbr_coef coefs[L502_ADC_RANGE_CNT];
} t_l502_eeprom_cbr_adc;

typedef struct
{
    t_l502_eeprom_cbr_hdr hdr;
    t_l502_cbr_coef coefs[L502_DAC_CH_CNT];
} t_l502_eeprom_cbr_dac;

typedef struct
{
    uint32_t sign;
    uint32_t size;
    uint32_t format;
    char name[L502_DEVNAME_SIZE];
    char serial[L502_SERIAL_SIZE];
    char res[64-12];
} t_l502_eeprom_hdr;


typedef struct
{
    t_l502_eeprom_hdr hdr;
    t_l502_eeprom_cbr_adc cbr_adc;
    t_l502_eeprom_cbr_dac cbr_dac;
    uint32_t crc;
} t_l502_descr;


/** биты регистра статуса */
typedef enum
{
    SST25_STATUS_BUSY   = 0x01,
    SST25_STATUS_WEL    = 0x02,
    SST25_STATUS_BP0    = 0x04,
    SST25_STATUS_BP1    = 0x08,
    SST25_STATUS_BP2    = 0x10,
    SST25_STATUS_BP3    = 0x20,
    SST25_STATUS_AAI    = 0x40,
    SST25_STATUS_BPL    = 0x80
} t_sst25_status_bits;


#endif // L502_EEPROM_H

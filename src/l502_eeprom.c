/***************************************************************************//**
  @file l502_eeprom.c
  @author Borisov Alexey <borisov@lcard.ru>
  @date   14.03.2011
  Файл содержит функции для работы с EEPROM модуля
 ******************************************************************************/
#include "l502_private.h"
#include "l502_eeprom.h"
#include "l502_fpga_regs.h"
#include "fast_crc.h"
#include "timer.h"

#include <stdlib.h>
#include <string.h>


#define L502_FlashWrite_TOUT  500
#define L502_FlashErase_TOUT  500


#define L502_EEPROM_BIG_SECTOR_SIZE    (64*1024)
#define L502_EEPROM_SMALL_SECTOR_SIZE  (4*1024)



#define L502_CHECK_ADDR(addr, size) ((addr+size)>L502_EEPROM_SIZE ? \
    L502_ERR_FLASH_INVALID_ADDR : (size==0) ? L502_ERR_FLASH_INVALID_SIZE : L502_ERR_OK)


/** Функция проверяет правильность информации о устройстве, записанной в EEPROM.
    При наличии верной инофрмации, из EEPROM считывается название устройства и
    серийный номер, а так же, при наличии, калибровочные коэффициенты */
int _check_eeprom(t_l502_hnd hnd)
{
    int err;
    uint32_t sign, size;


    /* проверяем признак правильного описателя в EEPROM и его размер */
    err = L502_FlashRead(hnd, L502_EEPROM_ADDR_DESCR, (uint8_t*)&sign, (uint32_t)sizeof(sign));
    if (!err)
        err = L502_FlashRead(hnd, L502_EEPROM_ADDR_DESCR+sizeof(sign), (uint8_t*)&size, (uint32_t)sizeof(size));

    if (!err)
    {
        if ((sign == L502_EEPROM_SIGN) && (size >= L502_DESCR_MIN_SIZE) && (size <= L502_DESCR_MAX_SIZE))
        {
            t_l502_descr* pdescr = (t_l502_descr*)malloc(size);
            /* читаем весь описатель */
            if (pdescr!=NULL)
            {
                err = L502_FlashRead(hnd, L502_EEPROM_ADDR_DESCR, (uint8_t*)pdescr, size);
            }
            else
            {
                err = L502_ERR_MEMORY_ALLOC;
            }

            /* сверяем crc */
            if (!err)
            {
                uint32_t crc, crc2;
                crc = CRC32_Block8(0, (uint8_t*)pdescr, (uint32_t)(size-sizeof(crc)));
                crc2 = *(uint32_t*)(&((uint8_t*)pdescr)[pdescr->hdr.size-4]);
                if (crc == crc2)
                {
                    hnd->info.devflags |= L502_DEVFLAGS_FLASH_DATA_VALID;
                    memcpy(hnd->info.serial, pdescr->hdr.serial, sizeof(pdescr->hdr.serial));
                    memcpy(hnd->info.name, pdescr->hdr.name, sizeof(pdescr->hdr.name));

                    if ((pdescr->cbr_adc.hdr.cbr_sign == L502_EEPROM_CBR_SIGN) &&
                            (pdescr->cbr_adc.hdr.format == L502_EEPROM_CBR_FROMAT) &&
                            (pdescr->cbr_adc.hdr.src == L502_EEPROM_CBR_SRC_ADC) &&
                            (pdescr->cbr_adc.hdr.range_cnt == L502_ADC_RANGE_CNT) &&
                            (pdescr->cbr_adc.hdr.channel_cnt == 1))
                    {
                        unsigned int i;

                        hnd->info.devflags |= L502_DEVFLAGS_FLASH_ADC_CALIBR_VALID;
                        for (i=0; (i < pdescr->cbr_adc.hdr.range_cnt) && (i < L502_ADC_RANGE_CNT)
                             && !err; i++)
                        {
                            err = L502_SetAdcCoef(hnd, i, pdescr->cbr_adc.coefs[i].k,
                                                    pdescr->cbr_adc.coefs[i].offs);
                        }
                    }


                    if ((pdescr->cbr_dac.hdr.cbr_sign == L502_EEPROM_CBR_SIGN) &&
                            (pdescr->cbr_dac.hdr.format == L502_EEPROM_CBR_FROMAT) &&
                            (pdescr->cbr_dac.hdr.src == L502_EEPROM_CBR_SRC_DAC) &&
                            (pdescr->cbr_dac.hdr.range_cnt == 1) &&
                            (pdescr->cbr_dac.hdr.channel_cnt == L502_DAC_CH_CNT))
                    {
                        unsigned int i;

                        hnd->info.devflags |= L502_DEVFLAGS_FLASH_DAC_CALIBR_VALID;
                        for (i=0; (i < pdescr->cbr_dac.hdr.channel_cnt) && (i < L502_ADC_RANGE_CNT)
                             && !err; i++)
                        {
                            err = L502_SetDacCoef(hnd, i, pdescr->cbr_dac.coefs[i].k,
                                                    pdescr->cbr_dac.coefs[i].offs);
                        }
                    }
                }
            }
            free(pdescr);
        }
    }
    return err;
}



static LINLINE int f_eeprom_rd_status(t_l502_hnd hnd, uint8_t* stat)
{
    uint32_t val;
    int err = _fpga_reg_read(hnd, L502_REGS_EEPROM_RD_STATUS, &val);
    *stat = (val>>24)&0xFF;
    return err;
}

static LINLINE int f_eeprom_wr_status(t_l502_hnd hnd, uint8_t stat)
{
    int err = _fpga_reg_write(hnd, L502_REGS_EEPROM_WR_STATUS_EN, 1);
    if (!err)
    {
        err = _fpga_reg_write(hnd, L502_REGS_EEPROM_WR_STATUS, stat);
    }
    return err;
}


static LINLINE int f_eeprom_wr_byte(t_l502_hnd hnd, uint32_t addr, uint8_t val)
{
    int err;
    t_timer tmr;
    uint8_t stat = SST25_STATUS_BUSY;

    /* разрешаем запись в EEPROM */
    err = _fpga_reg_write(hnd, L502_REGS_EEPROM_WR_EN, 1);
    if (!err)
    {
        err = _fpga_reg_write(hnd, L502_REGS_EEPROM_WR_BYTE, ((addr & 0xFFFFFF) << 8) | val);
        if (err)
            _fpga_reg_write(hnd, L502_REGS_EEPROM_WR_DIS, 1);
    }

    if (!err)
        timer_set(&tmr, L502_FlashWrite_TOUT*CLOCK_CONF_SECOND/1000);

    /* Ожидаем завершения записи */
    while (!err && (stat & SST25_STATUS_BUSY) && !timer_expired(&tmr))
    {
        err = f_eeprom_rd_status(hnd, &stat);
    }

    if (!err && (stat & SST25_STATUS_BUSY))
    {
        err = L502_ERR_FLASH_WRITE_TOUT;
    }

    return err;
}


LPCIE_EXPORT(int32_t) L502_FlashRead(t_l502_hnd hnd, uint32_t addr, uint8_t* data, uint32_t size)
{
    int err = L502_CHECK_HND(hnd);
    if (!err)
    {
        /** @todo check address */
        uint32_t last_addr, val;

        for (last_addr = addr+size; (addr < last_addr) && !err; addr+=sizeof(val))
        {
            err = _fpga_reg_write(hnd, L502_REGS_EEPROM_SET_RD_ADDR, (addr & 0xFFFFFF) << 8);
            if (!err)
                err = _fpga_reg_read(hnd, L502_REGS_EEPROM_RD_DWORD, &val);
            if (!err)
            {
                unsigned int i;
                for (i=0; (i < sizeof(val)) && size; i++, size--)
                {
                    *data++ = val & 0xFF;
                    val >>= 8;
                }
            }
        }
    }
    return err;
}


LPCIE_EXPORT(int32_t) L502_FlashWrite(t_l502_hnd hnd, uint32_t addr, const uint8_t* data, uint32_t size)
{
    int err = L502_CHECK_HND(hnd);
    if (!err)
    {
        err = L502_CHECK_ADDR(addr, size);
    }
    if (!err)
    {
        for (; size && !err; size--, addr++, data++)
        {
            err = f_eeprom_wr_byte(hnd, addr, *data);
        }
    }
    return err;
}


LPCIE_EXPORT(int32_t) L502_FlashErase(t_l502_hnd hnd, uint32_t addr, uint32_t size)
{
    int err = L502_CHECK_HND(hnd);
    if (!err)
    {
        err = L502_CHECK_ADDR(addr, size);
    }
    if (!err && ((addr & (L502_EEPROM_SMALL_SECTOR_SIZE-1)) ||
                 (size & (L502_EEPROM_SMALL_SECTOR_SIZE-1))))
    {
        err = L502_ERR_FLASH_SECTOR_BOUNDARY;
    }

    while(size && !err)
    {
        uint32_t er_size;
        /* разрешаем запись в EEPROM */
        err = _fpga_reg_write(hnd, L502_REGS_EEPROM_WR_EN, 1);
        if (!err)
        {
            uint8_t stat = SST25_STATUS_BUSY;
            t_timer tmr;
            /* проверяем - можем ли стереть целиком большой сектор или
               придется писать в мелкий */
            if ((size >= L502_EEPROM_BIG_SECTOR_SIZE) &&
                                !(size & (L502_EEPROM_BIG_SECTOR_SIZE-1)))
            {
                er_size = L502_EEPROM_BIG_SECTOR_SIZE;
                err = _fpga_reg_write(hnd, L502_REGS_EEPROM_ERASE_64K, addr<<8);
            }
            else
            {
                er_size = L502_EEPROM_SMALL_SECTOR_SIZE;
                err = _fpga_reg_write(hnd, L502_REGS_EEPROM_ERASE_4K, addr<<8);
            }

            if (!err)
                timer_set(&tmr, L502_FlashErase_TOUT*CLOCK_CONF_SECOND/1000);

            /* ожидаем завершения стирания */
            while (!err && (stat & SST25_STATUS_BUSY) && !timer_expired(&tmr))
            {
                err = f_eeprom_rd_status(hnd, &stat);
            }

            if (!err && (stat & SST25_STATUS_BUSY))
            {
                err = L502_ERR_FLASH_ERASE_TOUT;
            }

            /* запрещаем запись, если произошла ошибка. при успешном стирании
               запись будут запрещена атоматически */
            if (err)
                err = _fpga_reg_write(hnd, L502_REGS_EEPROM_WR_EN, 0);

            if (!err)
            {
                addr += er_size;
                size -= er_size;
            }
        }
    }

    return err;
}


LPCIE_EXPORT(int32_t) L502_FlashWriteEnable(t_l502_hnd hnd)
{
    int err = L502_CHECK_HND(hnd);
    if (!err)
    {
        err = f_eeprom_wr_status(hnd, SST25_STATUS_BP0 | SST25_STATUS_BP2);
    }
    return err;
}

LPCIE_EXPORT(int32_t) L502_FlashWriteDisable(t_l502_hnd hnd)
{
    int err = L502_CHECK_HND(hnd);
    if (!err)
    {
        err = f_eeprom_wr_status(hnd, SST25_STATUS_BP0 | SST25_STATUS_BP1 | SST25_STATUS_BP2);
    }
    return err;
}




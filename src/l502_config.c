#include "l502_private.h"
#include <string.h>
#include <stdlib.h>
#include <math.h>


#define CHECK_SYNC_MODE(mode) (((mode)) != L502_SYNC_INTERNAL) \
    && ((mode)!=L502_SYNC_EXTERNAL_MASTER) \
    && ((mode)!=L502_SYNC_DI_SYN1_RISE) \
    && ((mode)!=L502_SYNC_DI_SYN2_RISE) \
    && ((mode)!=L502_SYNC_DI_SYN1_FALL) \
    && ((mode)!=L502_SYNC_DI_SYN2_FALL) \
    ? L502_ERR_INVALID_SYNC_MODE : L502_ERR_OK

/* в предыдущей версии заголовка L502_SYNC_DI_SYN1_FALL и L502_SYNC_DI_SYN2_FALL были заданы не верно.
 * корректируем их, чтобы собранное со старым заголовком приложение могло работать корректно */
#define SYNC_MODE_CORRECT(mode) (mode = (mode == 4 ? L502_SYNC_DI_SYN1_FALL : mode == 5 ? L502_SYNC_DI_SYN2_FALL : mode))


static const uint32_t f_regadd_k[L502_ADC_RANGE_CNT] = {L502_REGS_IOARITH_K10,
                                                     L502_REGS_IOARITH_K5,
                                                     L502_REGS_IOARITH_K2,
                                                     L502_REGS_IOARITH_K1,
                                                     L502_REGS_IOARITH_K05,
                                                     L502_REGS_IOARITH_K02};

static const uint32_t f_regadd_offs[L502_ADC_RANGE_CNT] = {L502_REGS_IOARITH_B10,
                                                     L502_REGS_IOARITH_B5,
                                                     L502_REGS_IOARITH_B2,
                                                     L502_REGS_IOARITH_B1,
                                                     L502_REGS_IOARITH_B05,
                                                     L502_REGS_IOARITH_B02};






LPCIE_EXPORT(int32_t) L502_SetLChannel(t_l502_hnd hnd, uint32_t lch, uint32_t phy_ch,
                      uint32_t mode, uint32_t range, uint32_t avg)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err && (hnd->flags & _FLAGS_STREAM_RUN))
        err = L502_ERR_STREAM_IS_RUNNING;
    if (!err)
    {
        if ((mode != L502_LCH_MODE_COMM) && (mode != L502_LCH_MODE_DIFF) &&
                (mode != L502_LCH_MODE_ZERO))
        {
            err = L502_ERR_INVALID_LCH_MODE;
        }
        else if ((phy_ch >= 32) || ((mode != L502_LCH_MODE_COMM) && (phy_ch >= 16)))
        {
            err = L502_ERR_INVALID_LCH_PHY_NUMBER;
        }
        else if (range >= L502_ADC_RANGE_CNT)
        {
            err = L502_ERR_INVALID_LCH_RANGE;
        }
        else if (avg > L502_LCH_AVG_SIZE_MAX)
        {
            err = L502_ERR_INVALID_LCH_AVG_SIZE;
        }

        if (!err)
        {
            hnd->set.lch[lch].ch = phy_ch;
            hnd->set.lch[lch].range = range;
            hnd->set.lch[lch].mode = mode;
            hnd->set.lch[lch].avg = avg;
        }
    }
    return err;
}

LPCIE_EXPORT(int32_t) L502_SetLChannelCount(t_l502_hnd hnd, uint32_t lch_cnt)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err && (hnd->flags & _FLAGS_STREAM_RUN))
        err = L502_ERR_STREAM_IS_RUNNING;
    if (!err)
    {
        if (lch_cnt > L502_LTABLE_MAX_CH_CNT)
            err = L502_ERR_INVALID_LTABLE_SIZE;
        else
            hnd->set.lch_cnt = lch_cnt;
    }
    return err;
}

LPCIE_EXPORT(int32_t) L502_GetLChannelCount(t_l502_hnd hnd, uint32_t* lch_cnt)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err && (lch_cnt==NULL))
        err = L502_ERR_INVALID_POINTER;
    if (!err)
        *lch_cnt = hnd->set.lch_cnt;
    return err;
}

LPCIE_EXPORT(int32_t) L502_SetAdcFreqDivider(t_l502_hnd hnd, uint32_t adc_freq_div)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err && (hnd->flags & _FLAGS_STREAM_RUN))
        err = L502_ERR_STREAM_IS_RUNNING;
    if (!err)
    {
        if ((adc_freq_div==0) || (adc_freq_div > L502_ADC_FREQ_DIV_MAX))
            err = L502_ERR_INVALID_ADC_FREQ_DIV;
        else
            hnd->set.adc_freq_div = adc_freq_div;
    }
    return err;
}

LPCIE_EXPORT(int32_t) L502_SetAdcInterframeDelay(t_l502_hnd hnd, uint32_t delay)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err && (hnd->flags & _FLAGS_STREAM_RUN))
        err = L502_ERR_STREAM_IS_RUNNING;
    if (!err && (delay > L502_ADC_INTERFRAME_DELAY_MAX))
        err = L502_ERR_INVALID_INTERFRAME_DELAY;
    if (!err)
    {
        hnd->set.adc_frame_delay = delay;
    }
    return err;
}

LPCIE_EXPORT(int32_t) L502_SetDinFreqDivider(t_l502_hnd hnd, uint32_t din_freq_div)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err && (hnd->flags & _FLAGS_STREAM_RUN))
        err = L502_ERR_STREAM_IS_RUNNING;
    if (!err)
    {
        if ((din_freq_div==0) || (din_freq_div > L502_DIN_FREQ_DIV_MAX))
            err = L502_ERR_INVALID_ADC_FREQ_DIV;
        else
            hnd->set.din_freq_div = din_freq_div;
    }
    return err;
}













LPCIE_EXPORT(int32_t) L502_Configure(struct st_l502* hnd, uint32_t flags)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err && (hnd->flags & _FLAGS_STREAM_RUN))
        err = L502_ERR_STREAM_IS_RUNNING;
    if (!err)
    {
        /* Проверяем правильность установленной опорной частоты.
           Для внутренней может быть только одно из двух значений,
           для внешней главное, чтобы не принимало требуемой */
        if ((hnd->set.sync_mode==L502_SYNC_INTERNAL)
                && (hnd->set.ref_freq!=L502_REF_FREQ_2000KHZ)
                &&(hnd->set.ref_freq!=L502_REF_FREQ_1500KHZ))
        {
            err = L502_ERR_INVALID_REF_FREQ;
        }
        else if (hnd->set.ref_freq > L502_EXT_REF_FREQ_MAX)
        {
            err = L502_ERR_INVALID_REF_FREQ;
        }
    }
    if (!err)
    {
        if (hnd->mode == L502_MODE_FPGA)
        {
            uint32_t ch;
            /* записываем логическую таблицу */
            for (ch=0; (ch < hnd->set.lch_cnt) && !err; ch++)
            {
                uint32_t wrd = ((hnd->set.lch[ch].ch & 0xF) << 3) | (hnd->set.lch[ch].range & 0x7);

                if (hnd->set.lch[ch].mode == L502_LCH_MODE_ZERO)
                    wrd |= (3 << 7);
                else if (hnd->set.lch[ch].mode == L502_LCH_MODE_COMM)
                    wrd |= (hnd->set.lch[ch].ch & 0x10 ? 2 : 1) << 7;

                if (hnd->set.lch[ch].avg)
                {
                    uint32_t avg = hnd->set.lch[ch].avg;
                    if (avg > hnd->set.adc_freq_div)
                        avg = hnd->set.adc_freq_div;
                    wrd |= ((avg-1) & 0x7F) << 9;
                }

                err = _fpga_reg_write(hnd, L502_REGS_IOHARD_LTABLE + hnd->set.lch_cnt - 1 - ch, wrd);//L502_REGS_IOHARD_CT_SIZE - (ch+1), wrd);
            }
            if (!err)
                err = _fpga_reg_write(hnd, L502_REGS_IOHARD_LCH_CNT, hnd->set.lch_cnt-1);

            if (!err)
                err = _fpga_reg_write(hnd, L502_REGS_IOHARD_ADC_FREQ_DIV, hnd->set.adc_freq_div-1);

            if (!err)
                err = _fpga_reg_write(hnd, L502_REGS_IOARITH_ADC_FREQ_DIV, hnd->set.adc_freq_div-1);


            if (!err)
                err = _fpga_reg_write(hnd, L502_REGS_IOHARD_ADC_FRAME_DELAY, hnd->set.adc_frame_delay);

            if (!err)
            {
                err = _fpga_reg_write(hnd, L502_REGS_IOHARD_IO_MODE, (hnd->set.sync_mode & 0x7)
                                      | ((hnd->set.sync_start_mode&0x7)<<3)
                                      | ((hnd->set.ref_freq==L502_REF_FREQ_2000KHZ ?
                                              0 : 2) << 7)
                                      | (((hnd->set.dac_freq_div-1)&1)<<9));
            }

            if (!err)
                err = _fpga_reg_write(hnd, L502_REGS_IOHARD_DIGIN_FREQ_DIV, hnd->set.din_freq_div-1);


        }
        else if (hnd->mode == L502_MODE_DSP)
        {
            uint32_t ch;
            err = _set_bf_par(hnd, L502_BF_PARAM_LCH_CNT, &hnd->set.lch_cnt, 1);
            for (ch=0; !err && (ch < hnd->set.lch_cnt); ch++)
            {
                uint32_t ch_par[] = {ch, hnd->set.lch[ch].ch,
                                     hnd->set.lch[ch].mode,
                                     hnd->set.lch[ch].range,
                                     hnd->set.lch[ch].avg > hnd->set.adc_freq_div ?
                                        hnd->set.adc_freq_div : hnd->set.lch[ch].avg} ;
                err = _set_bf_par(hnd, L502_BF_PARAM_LCH, ch_par,
                                  sizeof(ch_par)/sizeof(ch_par[0]));
            }
            if (!err)
            {
                err = _set_bf_par(hnd, L502_BF_PARAM_ADC_FREQ_DIV,
                                  &hnd->set.adc_freq_div, 1);
            }
            if (!err)
            {
                err = _set_bf_par(hnd, L502_BF_PARAM_ADC_FRAME_DELAY,
                                  &hnd->set.adc_frame_delay, 1);
            }
            if (!err)
            {
                err = _set_bf_par(hnd, L502_BF_PARAM_REF_FREQ_SRC,
                                  &hnd->set.ref_freq, 1);
            }
            if (!err)
            {
                err = _set_bf_par(hnd, L502_BF_PARAM_SYNC_MODE,
                                  &hnd->set.sync_mode, 1);
            }
            if (!err)
            {
                err = _set_bf_par(hnd, L502_BF_PARAM_SYNC_START_MODE,
                                  &hnd->set.sync_start_mode, 1);
            }
            if (!err)
            {
                err = _set_bf_par(hnd, L502_BF_PARAM_DAC_FREQ_DIV,
                                  &hnd->set.dac_freq_div, 1);
            }
            if (!err)
            {
                err = _set_bf_par(hnd, L502_BF_PARAM_DIN_FREQ_DIV,
                                  &hnd->set.din_freq_div, 1);
            }

            if (!err)
            {
                err = L502_BfExecCmd(hnd, L502_BF_CMD_CODE_CONFIGURE, 0, NULL,
                                   0, NULL, 0, L502_BF_CMD_DEFAULT_TOUT, NULL);
            }

        }
        else
        {
            err = L502_ERR_INVALID_MODE;
        }
    }
    return err;
}


LPCIE_EXPORT(int32_t) L502_SetMode(t_l502_hnd hnd, uint32_t mode)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err && (hnd->flags & _FLAGS_STREAM_RUN))
        err = L502_ERR_STREAM_IS_RUNNING;
    if (!err && (mode==L502_MODE_DSP) &&
            !(hnd->info.devflags & L502_DEVFLAGS_BF_PRESENT))
    {
        err = L502_ERR_BF_NOT_PRESENT;
    }

    if (!err)
    {
        uint32_t val;
        err = _fpga_reg_read(hnd, L502_REGS_BF_CTL, &val);
        if (!err)
        {
            val &= ~(L502_REGBIT_BF_CTL_CLK_DIV_Msk | L502_REGBIT_BF_CTL_DBG_MODE_Msk | L502_REGBIT_BF_CTL_DSP_MODE_Msk);
            if (mode==L502_MODE_DSP)
                val |= L502_REGBIT_BF_CTL_DSP_MODE_Msk;
            else if (mode==L502_MODE_DEBUG)
                val |= L502_REGBIT_BF_CTL_DBG_MODE_Msk;
            else if (mode!=L502_MODE_FPGA)
                err = L502_ERR_INVALID_MODE;
        }

        if (!err)
            err = _fpga_reg_write(hnd, L502_REGS_BF_CTL, val);

        /* при переходе в режим DSP сбрасываем автомат HDMA */
        if (!err && (mode==L502_MODE_DSP))
            err = _fpga_reg_write(hnd, L502_REGS_BF_CMD, L502_BF_CMD_HDMA_RST);

        if (!err)
            hnd->mode = mode;
    }
    return err;
}

LPCIE_EXPORT(int32_t) L502_GetMode(t_l502_hnd hnd, uint32_t* mode)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err && (mode==NULL))
        err = L502_ERR_INVALID_POINTER;
    if (!err)
    {
        *mode = hnd->mode;
    }
    return err;
}


LPCIE_EXPORT(int32_t) L502_SetAdcFreq(t_l502_hnd hnd, double *f_acq, double *f_frame)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err && (hnd->flags & _FLAGS_STREAM_RUN))
        err = L502_ERR_STREAM_IS_RUNNING;
    if (!err && (f_acq==NULL))
        err = L502_ERR_INVALID_POINTER;
    if (!err)
    {
        double ref_freq = hnd->set.ref_freq;
        double set_freq = *f_acq;
        if (set_freq<=0)
            set_freq = ref_freq;
        hnd->set.adc_freq_div = (uint32_t)(ref_freq/set_freq+0.49);
        if (!hnd->set.adc_freq_div)
            hnd->set.adc_freq_div = 1;
        if (hnd->set.adc_freq_div > L502_ADC_FREQ_DIV_MAX)
            hnd->set.adc_freq_div = L502_ADC_FREQ_DIV_MAX;
        set_freq = ref_freq/hnd->set.adc_freq_div;
        *f_acq = set_freq;

        if (f_frame==NULL)
        {
            hnd->set.adc_frame_delay = 0;
        }
        else
        {
            if (*f_frame <= 0)
            {
                hnd->set.adc_frame_delay = 0;
            }
            else
            {
                int32_t frame_div = (int32_t)((ref_freq/(*f_frame)
                                    - hnd->set.lch_cnt*ref_freq/set_freq)+0.49);

                hnd->set.adc_frame_delay = frame_div <=0 ? 0 :
                          frame_div > L502_ADC_INTERFRAME_DELAY_MAX ?
                          L502_ADC_INTERFRAME_DELAY_MAX : frame_div;
            }
            *f_frame = 1./(hnd->set.lch_cnt/set_freq +
                           hnd->set.adc_frame_delay/ref_freq);
        }
    }
    return err;
}

LPCIE_EXPORT(int32_t) L502_GetAdcFreq(t_l502_hnd hnd, double *f_acq, double *f_frame)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err && (f_acq == NULL) && (f_frame==NULL))
        err = L502_ERR_INVALID_POINTER;
    if (!err)
    {
        double ref_freq = hnd->set.ref_freq;
        double set_freq = ref_freq/hnd->set.adc_freq_div;
        if (f_acq!=NULL)
        {
            *f_acq = set_freq;
        }
        if (f_frame!=NULL)
        {
            *f_frame = 1./(hnd->set.lch_cnt/set_freq +
                           hnd->set.adc_frame_delay/ref_freq);
        }
    }
    return err;
}


LPCIE_EXPORT(int32_t) L502_SetDinFreq(t_l502_hnd hnd, double *f_din)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err && (hnd->flags & _FLAGS_STREAM_RUN))
        err = L502_ERR_STREAM_IS_RUNNING;
    if (!err && (f_din==NULL))
        err = L502_ERR_INVALID_POINTER;
    if (!err)
    {
        double ref_freq = hnd->set.ref_freq;
        double set_freq = *f_din;
        if (set_freq<=0)
            set_freq = ref_freq;
        hnd->set.din_freq_div = (uint32_t)(ref_freq/set_freq+0.49);
        if (!hnd->set.din_freq_div)
            hnd->set.din_freq_div = 1;
        if (hnd->set.din_freq_div > L502_DIN_FREQ_DIV_MAX)
            hnd->set.din_freq_div = L502_DIN_FREQ_DIV_MAX;
        set_freq = ref_freq/hnd->set.din_freq_div;
        *f_din = set_freq;
    }
    return err;
}




LPCIE_EXPORT(int32_t) L502_SetRefFreq(t_l502_hnd hnd, uint32_t freq)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err && (hnd->flags & _FLAGS_STREAM_RUN))
        err = L502_ERR_STREAM_IS_RUNNING;
    if (!err)
        hnd->set.ref_freq = freq==0 ? 1 : freq;
    return err;
}

LPCIE_EXPORT(int32_t) L502_SetSyncMode(t_l502_hnd hnd, uint32_t sync_mode)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err)
    {
        SYNC_MODE_CORRECT(sync_mode);
    }

    if (!err)
        err = CHECK_SYNC_MODE(sync_mode);
    if (!err)
    {
        hnd->set.sync_mode = sync_mode;
    }
    return err;
}

LPCIE_EXPORT(int32_t) L502_SetSyncStartMode(t_l502_hnd hnd, uint32_t sync_start_mode)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err)
    {
        SYNC_MODE_CORRECT(sync_start_mode);
    }
    if (!err)
        err = CHECK_SYNC_MODE(sync_start_mode);
    if (!err)
    {
        hnd->set.sync_start_mode = sync_start_mode;
    }
    return err;
}



LPCIE_EXPORT(int32_t) L502_SetAdcCoef(t_l502_hnd hnd, uint32_t range, double k, double offs)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err && (hnd->flags & _FLAGS_STREAM_RUN))
        err = L502_ERR_STREAM_IS_RUNNING;
    if (!err)
    {
        if (range >= L502_ADC_RANGE_CNT)
        {
            err = L502_ERR_INVALID_LCH_RANGE;
        }
    }
    if (!err)
    {
        uint32_t kval = (uint32_t)(k*0x400000);
        uint32_t offs_val = (uint32_t)(-offs);

        if (hnd->mode == L502_MODE_FPGA)
        {
            err = _fpga_reg_write(hnd, f_regadd_k[range], kval);
            if (!err)
                err = _fpga_reg_write(hnd, f_regadd_offs[range], offs_val);
        }
        else if (hnd->mode == L502_MODE_DSP)
        {
            uint32_t wrds[3] = {range, kval, offs_val};
            err = _set_bf_par(hnd, L502_BF_PARAM_ADC_COEF, wrds, 3);
        }
        else
        {
            err = L502_ERR_INVALID_MODE;
        }
    }

    if (!err)
    {
        hnd->info.cbr.adc[range].k = k;
        hnd->info.cbr.adc[range].offs = offs;
    }

    return err;
}


LPCIE_EXPORT(int32_t) L502_GetAdcCoef(t_l502_hnd hnd, uint32_t range, double* k, double* offs)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err)
    {
        if (range >= L502_ADC_RANGE_CNT)
        {
            err = L502_ERR_INVALID_LCH_RANGE;
        }
    }
    if (!err)
    {
        *k = hnd->info.cbr.adc[range].k;
        *offs = hnd->info.cbr.adc[range].offs;
    }
    return err;
}



LPCIE_EXPORT(int32_t) L502_SetDacCoef(t_l502_hnd hnd, uint32_t ch, double k, double offs)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err && (ch!=L502_DAC_CH1) && (ch!=L502_DAC_CH2))
    {
        err = L502_ERR_INVALID_DAC_CHANNEL;
    }

    if (!err)
    {
        hnd->info.cbr.dac[ch].offs = offs;
        hnd->info.cbr.dac[ch].k = k;
    }
    return err;
}

LPCIE_EXPORT(int32_t) L502_GetDacCoef(t_l502_hnd hnd, uint32_t ch, double* k, double* offs)
{
    int32_t err = L502_CHECK_HND(hnd);
    if (!err && (ch!=L502_DAC_CH1) && (ch!=L502_DAC_CH2))
    {
        err = L502_ERR_INVALID_DAC_CHANNEL;
    }

    if (!err)
    {
        if (offs!=NULL)
            *offs = hnd->info.cbr.dac[ch].offs;
        if (k!=NULL)
            *k = hnd->info.cbr.dac[ch].k;
    }
    return err;
}


